<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternalBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->notNull();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
            $table->decimal('amount', 10, 2);
            $table->string('currency');
            $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_balances');
    }
}
