<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_applications', function (Blueprint $table) {
            $table->increments('id');
			
            $table->integer('user_id')->unsigned()->notNull();
            $table->integer('internal_balance_id')->unsigned()->notNull();
            $table->string('currency');
            $table->string('wallet_number');
            $table->integer('amount');
            $table->string('status');
            $table->timestamp('is_completed');

            $table->foreign('internal_balance_id')->references('id')->on('internal_balances')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_applications');
    }
}
