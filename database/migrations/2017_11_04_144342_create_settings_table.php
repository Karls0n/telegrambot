<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	 Schema::table('users', function (Blueprint $table) {
            $table->string('full_name')->affter('name');
        });


	Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('setting');
	    $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
 	 Schema::dropIfExists('settings');
	 
	 Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('full_name');
        });

    }
}
