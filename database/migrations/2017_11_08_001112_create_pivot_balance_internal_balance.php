<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotBalanceInternalBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_internal_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('balance_id')->unsigned();
	    $table->integer('internal_balance_id')->unsigned();

 	    $table->foreign('balance_id')->references('id')->on('balances')->onDelete('restrict');
 	    $table->foreign('internal_balance_id')->references('id')->on('internal_balances')->onDelete('restrict');

            $table->timestamps();
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_internal_balance');
    }
}
