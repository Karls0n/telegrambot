<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
   protected $table = 'wallets';

    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

}
