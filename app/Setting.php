<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
   /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];

    public function users() {
        return $this->belongsToMany('App\User')->withPivot("value")->withTimestamps();
    }

}
