<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'telegram_id', 'full_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function parent() {
        return $this->belongsTo('App\User', 'parent_id');
    }

    public function users() {
        return $this->hasMany('App\User', 'parent_id');
    }

    public function settings() {
        return $this->belongsToMany('App\Setting')->withPivot("value")->withTimestamps();
    }
	
    public function balances() {
        return $this->hasMany('App\Balance', 'user_id');
    }

    public function internal_balances() {
        return $this->hasMany('App\InternalBalance');
    }

	
    public function balance_histories() {
        return $this->hasMany('App\BalanceHistory');
    }
	
    public function wallets() {
        return $this->hasMany('App\Wallet', 'user_id');
    }

    public function withdrawal_applications() {
        return $this->hasMany('App\WithdrawalApplication');
    }
	
	public static function getChildrenByLine($user, $line) {
        if($line == 1) {
            return $user->users;
        }
        $childrens = new Collection();
        foreach($user->users as $node) {
            $childrens = $childrens->merge(self::getChildrenByLine($node, $line - 1));
        }
        return $childrens;
    }
	
	public static function getChildrensCountByLine($user, $line) {
        if($line == 1) {
            return $user->users->count();
        }
        $childrensNumber = 0;
        foreach($user->users as $node) {
            $childrensNumber += self::getChildrensCountByLine($node, $line - 1);
        }
        return $childrensNumber;
    }
	
	public static function getActiveChildrenCount($user) {

        $childrensNumber = $user->users()->whereHas('balances')->count();

        foreach($user->users as $node) {
            $childrensNumber += self::getActiveChildrenCount($node);
        }
        return $childrensNumber;
    }
	
	public static function getTurnoverByLine($user, $line) {
		
		$users = self::getChildrenByLine($user, $line);
		
		$turnover = 0;
		
		if($users) {
			foreach ($users as $user) {
				$turnover += $user->balances->sum('amount');
			}
		}
		
		return $turnover;
	}
	
	public static function getProfitByLine($user, $line) {
		
		$profit = BalanceHistory::where('user_id', $user->id)->where('source', 'Партнерская программа ур.'.$line)->sum('amount');
		
		return $profit;
	}
	
	public static function getTeamTurnover($user) {

		$turnover = 0;
		for($i=1; $i <=3; $i++) {
			$users = self::getChildrenByLine($user, $i);
			if($users) {
				foreach ($users as $user) {
					$turnover += $user->balances->sum('amount');
				}
			}
		}
        
		
		return $turnover;
    }
	
	public static function getMaxDepo($user) {

		$maxDepo = 0;
		foreach($user->users as $child) {
			if($child->balances) {
				$maxPerUser = $child->balances->sortByDesc('amount')->pluck('amount')->take(1);		
				if($maxPerUser->first() > $maxDepo) {
					$maxDepo = $maxPerUser->first();
				}
			}
		}

	return round($maxDepo);
    }

    public function refererIsValid($referer_id)
    {
        $referer = User::find($referer_id);

        if($referer->id != $this->id){
            if($referer->parent) {
                if($referer->parent->id != $this->id){
                    $this->refererIsValid($referer->parent->id);
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }

        return true;
    }

}
