<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalanceHistory extends Model
{
     protected $table = 'balance_histories';

    protected $guarded = ['id'];

    public function historyable()
    {
        return $this->morphTo();
    }

    public function getTypeOperation()
    {
        if ($this->amount < 0) {
            return 'Расход';
        } else {
            return 'Приход';
        }
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

}
