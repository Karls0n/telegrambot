<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        /*'481716756:AAHBcGbXg_PCBjcrd4L6VEEyF7ke8QoDgNU', '/replenish/perfect-money/success', '/replenish/perfect-money/failed', '/replenish/perfect-money/status',*/
        '520768351:AAFxFf-VD6VxC_O2-T_caiRcfZ9j1rHxVkk', '/replenish/perfect-money/success', '/replenish/perfect-money/failed', '/replenish/perfect-money/status'
    ];
}
