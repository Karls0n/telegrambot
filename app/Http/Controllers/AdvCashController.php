<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\User;

class AdvCashController extends Controller
{
    public function index($id, $money)
    {
	$user = User::where('telegram_id', $id)->first();
	$setting = $user->settings()->where('setting', 'command')->first()->pivot;
	$setting->value = '';
	$setting->save();
	
	$ac_email = 'frs.official.info@gmail.com';
	$sci_name = 'FRS Project';
	$ac_amount = $money;
	$ac_currency = 'USD';
	$order_id = rand(99999,999999);
	$str = "$ac_email:$sci_name:$ac_amount:$ac_currency:1q2w3e4r5tt5r4e3w2q1:$order_id";
	$ac_sign = hash('sha256', $str);

    	return view('replenish.advcash', compact('id', 'money', 'order_id', 'ac_email', 'sci_name', 'ac_amount', 'ac_currency', 'ac_sign'));
    }

    public function status(Request $request, $id)
    {
		Log::info($request->all());
		Log::info($id);
		exit;
		
		if ($request->ip() == env('PM_SERVER_ADDRESS')){
		
			$txt =serialize($request->all()).', IP:'.$request->ip().', HASH: '.$request->V2_HASH;
            $hash = $request->V2_HASH;
            $ccy = $request->PAYMENT_UNITS;
            $order_id = $request->payment_id;

            $string=
                $request->PAYMENT_ID.':'.$request->PAYEE_ACCOUNT.':'.
                $request->PAYMENT_AMOUNT.':'.$request->PAYMENT_UNITS.':'.
                $request->PAYMENT_BATCH_NUM.':'.
                $request->PAYER_ACCOUNT.':'.strtoupper(md5(env('ALTERNATE_PHRASE_HASH'))).':'.
                $request->TIMESTAMPGMT;

            $response_hash=strtoupper(md5($string));

            if($hash==$response_hash){
                $payment_units = array ('USD');
                if (!in_array($request->PAYMENT_UNITS, $payment_units)){
                    header('HTTP/1.0 424 Failed Dependency');
                    echo 'Not allowed Currecnty Type!';
                    exit;
                }
				
				$user = \App\User::where('telegram_id', $request->USERID)->first();

				/* if($balance = \App\Balance::where('user_id', $user->id)->where('currency', $request->PAYMENT_UNITS)->first()) {
					$balance->increment('amount', $request->PAYMENT_AMOUNT);
				} else {
				} */
				
				if(!$int_balance = \App\InternalBalance::where('user_id', $user->id)->where('currency', $request->PAYMENT_UNITS)->first()) {			
					$int_balance = new \App\InternalBalance([
					'user_id' => $user->id,
					'amount' => 0,
					'currency' => $request->PAYMENT_UNITS
					]);
					
					$int_balance->save();
				} else {
					$int_balance = \App\InternalBalance::where('user_id', $user->id)->where('currency', $request->PAYMENT_UNITS)->first();
				}
								
				
					$balance = new \App\Balance([
					'user_id' => $user->id,
					'amount' => $request->PAYMENT_AMOUNT,
					'currency' => $request->PAYMENT_UNITS
					]);
					
					$balance->save();
					
					$int_balance->balances()->attach($balance->id);
				

				$history = new \App\BalanceHistory([
					'user_id' => $user->id,
					'amount' => $request->PAYMENT_AMOUNT,
					'currency' => $request->PAYMENT_UNITS,
					'source' => 'Perfect Money Merchant',
					'description' => 'Размещение нового депозита'
				]);

				$balance->history()->save($history);

                // проводка по маркетингу
                for ($i=1; $i <= 3; $i++) {

                    if ($i == 1) {
                        $parent = $balance->user->parent;
                        $ref_percent = 3/100;
                    }
                    if ($i == 2 && $parent) {
                        $parent = $balance->user->parent->parent;
                        $ref_percent = 2.5/100;
                    }
                    if ($i == 3 && $parent) {
                        $parent = $balance->user->parent->parent->parent;
                        $ref_percent = 2/100;
                    }


                    if ($parent) {
                        if($parent->balance) {

                            $award = round($balance->amount * $ref_percent, 3);
                            $parent_balance = $parent->balance->internal_balances()->where('currency', $balance->currency)->first();

                            if($parent_balance) {
                                $parent_balance->increment('amount', $award);

                                $history = new \App\BalanceHistory([
                                    'user_id' => $parent->id,
                                    'amount' => $award,
                                    'currency' => $balance->currency,
                                    'source' => 'Реферальная система',
                                    'description' => 'Начисление реферальных с партнера '.$i.' линии '.$balance->user->name.''
                                ]);

                                $parent_balance->history()->save($history);
                            }
                        }
                    }
                }


                Log::info('Платеж успешно завершон!');
                header('HTTP/1.0 200 OK');
                echo 'Success!';
                exit;

            }else {
                header('HTTP/1.0 403 Forbidden');
                echo 'Forbidden! Hashes mismatch!';
                exit;
            }
} else {
	header('HTTP/1.0 403 Forbidden');
    echo 'You are from forbidden IP!';
	exit;
}
    }
}
