<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;
use App\User;
use App\News;
use App\InternalBalance;
use App\Setting;
use App\Wallet;
use App\Balance;
use App\BalanceHistory;
use App\WithdrawalApplication;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Carbon\Carbon;

class WebhookController extends Controller
{

    public function getStateCommand($chat_id)
    {
        if ($this->getCommandValue($chat_id)) {
            return true;
        }

        return false;
    }

    public function setReinvest($chat_id, $money, $telegram)
    {
        $user = User::where('telegram_id', $chat_id)->first();

        $int_balance = InternalBalance::where('user_id', $user->id)->where('currency', 'USD')->first();
// money_limit
        if ($int_balance->amount >= $money && $money >= 0.01) {
            $balance = new Balance([
                'user_id' => $user->id,
                'amount' => $money,
                'currency' => 'USD'
            ]);

            $balance->save();

            $int_balance->amount -= $money;
            $int_balance->save();

            $int_balance->balances()->attach($balance->id);


            $history = new BalanceHistory([
                'user_id' => $user->id,
                'amount' => $money,
                'currency' => 'USD',
                'source' => 'Реинвест. Счёт №' . $int_balance->id,
                'description' => 'Размещение нового депозита'
            ]);

            $balance->history()->save($history);

            $reply = "Поздравляем! Вы разместили новый депозит на сумму <strong> $money USD</strong>";
            $keyboard = [[hex2bin('F09F92B0') . " Мои депозиты", hex2bin('F09F92B8') . " Вывести", hex2bin('F09F92B2') . " Реинвест"], [hex2bin('F09F938A') . " Операции", hex2bin('F09F939D') . " Заявки на вывод"], [hex2bin('F09F8FA0') . " Главное меню"]];
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            $this->clearCommand(User::where('telegram_id', $chat_id)->first()->name);

        } else {
            $reply = "Не достаточно средств для совершения операции.";
            $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            $this->clearCommand(User::where('telegram_id', $chat_id)->first()->name);
        }
    }

    public function getCommandValue($chat_id)
    {
        if ($user = User::where("telegram_id", $chat_id)->first()) {
            $pivot = $user->settings()->where("setting", "command")->first()->pivot;
            if ($value = $pivot->value) {
                return $value;
            }
        }

        return false;
    }

    public function clearCommand($chat_id)
    {
        if ($user = User::where("telegram_id", $chat_id)->first()) {
            $pivot = $user->settings()->where("setting", "command")->first()->pivot;
            $pivot->value = '';
            $pivot->save();
        }
    }

    public function setCommand($chat_id, $command)
    {
        if ($user = User::where("telegram_id", $chat_id)->first()) {
            //Log::info($user);
            $pivot = $user->settings()->where("setting", "command")->first()->pivot;
            $pivot->value = $command;
            $pivot->save();

        } else return false;
    }

    public function getUser($chat_id, $telegram)
    {

        if ($user = User::where('telegram_id', $chat_id)->first()) {
            /*Log::debug('TelegramController.getUser', [
                'action' => 'Запрос на авторизацию получен: ' . $chat_id
            ]);*/
            return $user;
        } else {

            return null;
        }
    }

    public function sendMessage($telegram, $chat_id, $reply, $reply_markup = null, $disable_web_page_preview = null)
    {

        try {

            $telegram->sendMessage([
                'chat_id' => $chat_id,
                'text' => $reply,
                'parse_mode' => 'HTML',
                'reply_markup' => $reply_markup ?: false,
                'disable_web_page_preview' => $disable_web_page_preview ?: false,
            ]);

        } catch (TelegramResponseException $e) {

            $errorData = $e->getResponseData();

            if ($errorData['ok'] === false) {
                $telegram->sendMessage([
                    'chat_id' => env('TELEGRAM_ADMIN_ID'),
                    'text' => 'There was an error for a user. ' . $errorData['error_code'] . ' ' . $errorData['description'],
                ]);
            }
        }

    }

    public function withdrawMoney($chat_id, $money, $telegram, $wallet)
    {
        $user = User::where('telegram_id', $chat_id)->first();

        $int_balance = InternalBalance::where('user_id', $user->id)->where('currency', 'USD')->first();

        if ($int_balance->amount >= $money) {
            $withdrawal_app = new WithdrawalApplication([
                'user_id' => $user->id,
                'internal_balance_id' => $int_balance->id,
                'currency' => $wallet->currency,
                'wallet_number' => $wallet->title . ' ' . $wallet->number,
                'amount' => $money,
                'status' => 'new',
                'is_completed' => null
            ]);

            $withdrawal_app->save();

            $int_balance->amount -= $money;
            $int_balance->save();

            // $int_balance->withdrawal_app()->attach($withdrawal_app->id);


            $history = new BalanceHistory([
                'user_id' => $user->id,
                'amount' => $money,
                'currency' => 'USD',
                'source' => 'Баланс',
                'description' => 'Блокировка средств по заявке на вывод. Номер заявки №' . $int_balance->id,
            ]);

            //$withdrawal_app->history()->save($history);

            $reply = "Поздравляем! Вы подали заявку на вывод <strong> $money USD</strong>";
            $keyboard = [[hex2bin('F09F92B0') . " Мои депозиты", hex2bin('F09F92B8') . " Вывести", hex2bin('F09F92B2') . " Реинвест"], [hex2bin('F09F938A') . " Операции", hex2bin('F09F939D') . " Заявки на вывод"], [hex2bin('F09F8FA0') . " Главное меню"]];
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            $this->clearCommand(User::where('telegram_id', $chat_id)->first()->name);

        } else {
            $reply = "Не достаточно средств для совершения операции.";
            $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            $this->clearCommand(User::where('telegram_id', $chat_id)->first()->name);
        }
    }


    public function index()
    {

        $telegram = new Api(env('TELEGRAM_BOT_TOKEN')); //Устанавливаем токен, полученный у BotFather
        $result = $telegram->getWebhookUpdates(); //Передаем в переменную $result полную информацию о сообщении пользователя

        /*Log::debug('TelegramController.process', [
            'update' => $result
        ]);*/

        $message = $result->getMessage();

        $callback = $result->getCallbackQuery();
        $edited = $result->getEditedMessage();

        $mainMenu = [[hex2bin('F09F92B5') .  " Финансы", hex2bin('F09F91AA') . " Партнеры"], [hex2bin('F09F92B0') . " Мои депозиты", hex2bin('F09F94A8') . " Настройки"], [hex2bin('F09F93B0') . " Новости", hex2bin('E29C8F') . " Общий чат"],[hex2bin('f09f938c') . " О нас", hex2bin('e29d93') . " Вопрос\Ответ"], [hex2bin('F09F9396') . " Маркетинг", hex2bin('F09F998B') . " Поддержка"]];

        if ($message) {


            $user = $message->getFrom();
            $chat_id = $user->getId();
            $text = $message->getText();
            $name = $user->getUsername() ?: '';
            $last_name = $user->getLastName() ?: '';
            $first_name = $user->getFirstName() ?: '';
            $full_name = $first_name . ' ' . $last_name;
      
/*
		 $reply = "В данный момент сервис находится на техническом обслуживании. \n
Приносим извинения за временные неудобства. \n
Спасибо за понимание.";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    return 'ok';*/




  } elseif ($callback) {
            $message = $callback->getData();
            if ($message == 'back') {
                $reply = "Выберите действие \n";
                $keyboard = $mainMenu;
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $callback->getFrom()->getId(), $reply, $reply_markup);
                //$telegram->sendMessage([ 'chat_id' => $callback->getFrom()->getId(), 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
            } elseif ($message == 'reinvest_50') {
                $this->setReinvest($callback->getFrom()->getId(), 50, $telegram);
            } elseif ($message == 'reinvest_100') {
                $this->setReinvest($callback->getFrom()->getId(), 100, $telegram);
            } elseif ($message == 'reinvest_150') {
                $this->setReinvest($callback->getFrom()->getId(), 150, $telegram);
            } elseif ($message == 'reinvest_200') {
                $this->setReinvest($callback->getFrom()->getId(), 200, $telegram);
            } elseif ($message == 'reinvest_300') {
                $this->setReinvest($callback->getFrom()->getId(), 300, $telegram);
            } elseif ($message == 'reinvest_400') {
                $this->setReinvest($callback->getFrom()->getId(), 400, $telegram);
            } elseif ($message == 'reinvest_500') {
                $this->setReinvest($callback->getFrom()->getId(), 500, $telegram);
            } elseif ($message == 'reinvest_600') {
                $this->setReinvest($callback->getFrom()->getId(), 600, $telegram);
            } elseif ($message == 'reinvest_700') {
                $this->setReinvest($callback->getFrom()->getId(), 700, $telegram);
            } elseif ($message == 'reinvest_800') {
                $this->setReinvest($callback->getFrom()->getId(), 800, $telegram);
            } elseif ($message == 'reinvest_900') {
                $this->setReinvest($callback->getFrom()->getId(), 900, $telegram);
            } elseif ($message == 'reinvest_1000') {
                $this->setReinvest($callback->getFrom()->getId(), 1000, $telegram);
            } elseif ($message == 'reinvest_all') {
                $user = User::where('telegram_id', $callback->getFrom()->getId())->first();
                $int_balance = InternalBalance::where('user_id', $user->id)->where('currency', 'USD')->first();
                if ($int_balance->amount > 0) {
                    $this->setReinvest($callback->getFrom()->getId(), $int_balance->amount, $telegram);
                }
            }
            return 'ok'; //if callback query
        } else {
            return 'ok'; //if edited_message
        }

        if (starts_with($text, '/start') && strlen($text) > 6) {

            list($str, $referer_id) = explode(" ", $text);

            $user = $this->getUser($chat_id, $telegram);

            if (!$user) {
                $setting = Setting::where("setting", "command")->first();
                if ($name) {
                    $user = new User();
                    $user->email = $chat_id . "@frsproject.com";
                    $user->telegram_id = $chat_id;
                    $user->name = "@" . $name;
                    $user->full_name = mb_convert_encoding(substr($full_name, 0, 200), "UTF-8", "UTF-8");
                    $user->password = bcrypt('123456');
                    $user->save();
                    $user->settings()->attach($setting->id);
                    $reply = "Поздравляем с регистрацией в <strong>FRS Project</strong>. \n";
                } else {
                    $reply = "Ошибка! К регистрации допускаются только пользователи с заполненым полем <strong>username</strong>.
								 Пожалуйста, в настройках своего аккаунта заполните соответсвующее поле и повторите регистрацию.";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                    return 'ok';
                }
            }


            $referer = $this->getUser($referer_id, $telegram);

            //Log::info('Строка 129 ' . $referer);

            if (!$referer) {

                $reply = "Пользователя под номером <strong>$referer_id</strong> не найдено.\nВы можете самостоятельно установить пригласителя в разделе <strong>Партнеры</strong> => <strong>Мой пригласитель</strong> => <strong>Установить пригласителя</strong>";

            }

            if ($referer && $user) {


               // Log::info('Юзер для закрепления реферера найден');

                if ($user->id == $referer->id) {
                    //Log::info('Юзер устанавливает сам себя');
                    $reply = "Запрещено устанавливать собственный аккаунт в качестве пригласителя";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    return 'ok';
                }

                if($user->refererIsValid($referer->id)){
                    $user->parent_id = $referer->id;
                    $user->save();
                    $reply = "Вашим пригласителем установлен <strong>$referer->name ( $referer_id )</strong>.\nВыберите дальнейшее действие";

                }else{
                    //Log::info('Юзер устанавливает себе родителя из цепочки.');
                    $reply = "Нельзя устаовить это пользователя пригласителем.";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    return 'ok';
                }

            }

            $keyboard = $mainMenu;
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            return 'ok';
        }

        if ($name && $this->getStateCommand($chat_id)) {
            if ($this->getCommandValue($chat_id) == 'set_refer') {
                if ($refer = User::where('name', $text)->first() && $user = $this->getUser($chat_id, $telegram)) {
                    $refer = User::where('name', $text)->first();
                    if ($user->id != $refer->id and $user->refererIsValid($refer->id)){
                        $user->parent_id = $refer->id;
                        $reply = "Вашим пригласителем установлен: " . $text;
                    }else{
                        $reply = "Нельзя установить этого пользователя в качестве пригласителя.";
                    }
                    $user->save();

                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                    $this->clearCommand($chat_id);
                } elseif ($text == "Установить пригласителя позже") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие \n";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $reply = "Пользователь с ником <b>\"" . $text . "\"</b> не найден.";

                    $this->sendMessage($telegram, $chat_id, $reply);
                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply ]);
                }
            } elseif ($this->getCommandValue($chat_id) == 'perfect-money' OR $this->getCommandValue($chat_id) == 'advcash') {
                if ($text == hex2bin('F09F8FA0') . " Главное меню" OR $text == hex2bin('F09F92B0') . " Мои депозиты") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    //if (is_numeric($text) and $text >= 10)
                    if ($text >= 0.01)
                        $sum = $text;
                    else
                        $sum = 100;
					$payment = $this->getCommandValue($chat_id);
                    $reply = "Для подтверждения пополнения на сумму <strong>\"$sum$\"</strong> нажмите на кнопку <b>\"Подтвердить\"</b>";
                    $inlineLayout = [[
                        Keyboard::inlineButton(['text' => 'Подтвердить', 'url' => "https://frs-project.com/replenish/$payment/$chat_id/$sum"]),
                    ]];
                    $inline_keyboard = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
                    $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);
                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $inline_keyboard ]);

                    $this->clearCommand($chat_id);
                }
            } elseif ($this->getCommandValue($chat_id) == 'reinvest_money') {
                if ($text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    //if (is_numeric($text) and $text >= 10)
                    if ($text >= 0.01)
                        $sum = $text;
                    else
                        $sum = 100;
                    $this->setReinvest($chat_id, $sum, $telegram);
                }
            } elseif ($this->getCommandValue($chat_id) == 'withdraw_from_perfect') {
                if ($text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $wallet = User::where('telegram_id', $chat_id)->first()->wallets()->where('title', 'PerfectMoney')->first();
                    $this->withdrawMoney($chat_id, $text, $telegram, $wallet);
                }
            } elseif ($this->getCommandValue($chat_id) == 'withdraw_from_advcash') {
                if ($text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $wallet = User::where('telegram_id', $chat_id)->first()->wallets()->where('title', 'AdvCash')->first();
                    $this->withdrawMoney($chat_id, $text, $telegram, $wallet);
                }
            } elseif ($this->getCommandValue($chat_id) == 'withdraw_from_ethereum') {
                if ($text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $wallet = User::where('telegram_id', $chat_id)->first()->wallets()->where('title', 'Ethereum')->first();
                    $this->withdrawMoney($chat_id, $text, $telegram, $wallet);
                }
            } elseif ($this->getCommandValue($chat_id) == 'withdraw_from_bitcoin') {
                if ($text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $wallet = User::where('telegram_id', $chat_id)->first()->wallets()->where('title', 'Bitcoin')->first();
                    $this->withdrawMoney($chat_id, $text, $telegram, $wallet);
                }
            } elseif ($this->getCommandValue($chat_id) == 'set_advcash') {
                if ($text == "Отмена" or $text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $user = User::where('telegram_id', $chat_id)->first();
                    $wallet = $user->wallets()->where('title', 'AdvCash')->first();
                    if ($wallet) {
                        $wallet->number = $text;
                        $wallet->save();
                    } else {
                        $wallet = new Wallet([
                            'user_id' => $user->id,
                            'title' => 'AdvCash',
                            'currency' => 'USD',
                            'number' => $text
                        ]);
                        $wallet->save();
                    }
                    $reply = "Вы ввели " . $text;
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    $this->clearCommand($chat_id);
                }
            } elseif ($this->getCommandValue($chat_id) == 'set_perfect') {
                if ($text == "Отмена" or $text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {

                    $user = User::where('telegram_id', $chat_id)->first();
                    $wallet = $user->wallets()->where('title', 'PerfectMoney')->first();
                    if ($wallet) {
                        $wallet->number = $text;
                        $wallet->save();
                    } else {
                        $wallet = new Wallet([
                            'user_id' => $user->id,
                            'title' => 'PerfectMoney',
                            'currency' => 'USD',
                            'number' => $text
                        ]);
                        $wallet->save();
                    }
                    $reply = "Вы ввели " . $text;
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    $this->clearCommand($chat_id);
                }
            } elseif ($this->getCommandValue($chat_id) == 'set_btc') {
                if ($text == "Отмена" or $text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {

                    $user = User::where('telegram_id', $chat_id)->first();
                    $wallet = $user->wallets()->where('title', 'Bitcoin')->first();
                    if ($wallet) {
                        $wallet->number = $text;
                        $wallet->save();
                    } else {
                        $wallet = new Wallet([
                            'user_id' => $user->id,
                            'title' => 'Bitcoin',
                            'currency' => 'BTC',
                            'number' => $text
                        ]);
                        $wallet->save();
                    }
                    $reply = "Вы ввели " . $text;
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    $this->clearCommand($chat_id);
                }
            } elseif ($this->getCommandValue($chat_id) == 'set_eth') {
                if ($text == "Отмена" or $text == hex2bin('F09F8FA0') . " Главное меню") {
                    $this->clearCommand($chat_id);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {

                    $user = User::where('telegram_id', $chat_id)->first();
                    $wallet = $user->wallets()->where('title', 'Ethereum')->first();
                    if ($wallet) {
                        $wallet->number = $text;
                        $wallet->save();
                    } else {
                        $wallet = new Wallet([
                            'user_id' => $user->id,
                            'title' => 'Ethereum',
                            'currency' => 'ETH',
                            'number' => $text
                        ]);
                        $wallet->save();
                    }
                    $reply = "Вы ввели " . $text;
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    $this->clearCommand($chat_id);
                }
            }
        } elseif ($text) {
            if ($text == "/start") {
                if ($this->getUser($chat_id, $telegram)) {
                    $reply = "Вы уже зарегистрированы в системе. \n С возвращением в <strong>FRS Project</strong>";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $setting = Setting::where("setting", "command")->first();
                    if ($name) {
                        $user = new User();
                        $user->email = $chat_id . "@frsproject.com";
                        $user->telegram_id = $chat_id;
                        $user->name = "@" . $name;
                        $user->full_name =  mb_convert_encoding(substr($full_name, 0, 200), "UTF-8", "UTF-8");
                        $user->password = bcrypt('123456');
                        $user->save();
                        $user->settings()->attach($setting->id);
                        $reply = "Регистрация прошла успешно. \n";

                        $reply .= "Привет <b>$full_name</b>. Добро пожаловать в FRS Project.
			📢Партнерские бонусы FRS.
🥇1 линия -  3% (10%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
🥈2 линия - 2.5%(7%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
🥉3 линия - 2%(5%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
Начисления каждый день.
Вывод каждое воскресенье.
Доступные способы ВВОДА:  Bitcoin, ADVcash, Perfect Money, Ethereum

			Отправьте ник того, кто пригласил Вас (вида @FRSProject)";

                        $keyboard = [["Установить пригласителя позже"], [hex2bin('F09F92B5') .  " Финансы", hex2bin('F09F94A8') . " Настройки"], ["Банеры", hex2bin('F09F94A8') . " Настройки"]];
                        $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                        $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                        //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

                        $this->setCommand($chat_id, "set_refer");
                    } else {
                        $reply = "Ошибка! К регистрации допускаются только пользователи с заполненым полем <strong>username</strong>.
								 Пожалуйста, в настройках своего аккаунта заполните соответсвующее поле и повторите регистрацию.";
                        $this->sendMessage($telegram, $chat_id, $reply);
                        return 'ok';

                        //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply ]);
                    }
                }
            } elseif ($text == hex2bin('F09F91A4') . " Мой пригласитель") {
                //Log::info($chat_id);
                //Log::info($this->getUser($chat_id, $telegram));
                $user = $this->getUser($chat_id, $telegram);

                if ($user) {
                    if ($user->parent) {
                        $referer = $user->parent;
                        $referer_name = $referer->name;
                        $reply = "Ваш пригласитель <strong>$referer_name</strong>\n Выберите дальнейшее действие";
                    } else {
                        $reply = "У вас еще не установлен пригласитель.";
                    }
                    $keyboard = [["Установить пригласителя"], [hex2bin('F09F8FA0') . " Главное меню"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $reply = "<strong>Ошибка! Неавторизированное действие. Пользователь: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Ошибка! Неавторизированное действие. Повторите вход в систему или зарегистрируйтесь</strong> \n";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }

            } elseif ($text == hex2bin('F09F92B5') .  " Финансы") {
                $user = $this->getUser($chat_id, $telegram);
                if ($user) {
                    $balance = $user->balances()->where('currency', 'USD')->first() ? $user->balances()->where('currency', 'USD')->sum('amount') : '0.00';
                    $int_balance = $user->internal_balances()->where('currency', 'USD')->first() ? $user->internal_balances()->where('currency', 'USD')->first()->amount : '0.00';
                    $reply = "
				<strong>Ваш Банк</strong>
				
				Ваш баланс:
				<strong>USD - $int_balance $ </strong>\n
				Сумма ваших депозитов:
				<strong>USD - $balance $</strong> \n
				Заработано в проекте:
				<strong>USD - 0.00 $</strong>";
                    if ($user->internal_balances()->where('currency', 'USD')->first()) {
                        $keyboard = [[hex2bin('F09F92B0') . " Мои депозиты", hex2bin('F09F92B8') . " Вывести"], [hex2bin('F09F938A') . " Операции", hex2bin('F09F939D') . " Заявки на вывод"], [ hex2bin('F09F92B2') . " Реинвест", hex2bin('F09F8FA0') . " Главное меню"]];
                    } else {
                        $keyboard = [[hex2bin('F09F92B0') . " Мои депозиты"], [hex2bin('F09F938A') . " Операции", hex2bin('F09F939D') . " Заявки на вывод"], [hex2bin('F09F8FA0') . " Главное меню"]];
                    }
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

                } else {
                    $reply = "<strong>Ошибка! Неавторизированное действие. Пользователь: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Ошибка! Неавторизированное действие. Повторите вход в систему или зарегистрируйтесь</strong> \n";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }

            } elseif ($text == "Пополнить") {
                $reply = "Выберите платежную систему";
                $keyboard = [["Perfect Money", "Advanced Cash"], [hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            } elseif ($text == hex2bin('F09F91AA') . " Партнеры") {
                $user = $this->getUser($chat_id, $telegram);
                if ($user) {
                    $reply = "<strong>Партнерская программа</strong>\n
						 Всего партнеров в Вашей команде: <strong>" . $user->users->count() . "</strong>
						 Активных партнеров: <strong> " . User::getActiveChildrenCount($user) . " </strong>
						 
						 Всего инвестировано в Вашей команде: <strong>" . User::getTeamTurnover($user) . " USD</strong>
						 Самый большой депозит: <strong>" . User::getMaxDepo($user) . " USD</strong>

						 Всего партнеров в 1-й линии: <strong> " . User::getChildrensCountByLine($user, 1) . "  </strong>
						 Всего партнеров в 2-й линии: <strong> " . User::getChildrensCountByLine($user, 2) . " </strong>
						 Всего партнеров в 3-й линии: <strong> " . User::getChildrensCountByLine($user, 3) . " </strong>
						 
						 Инвестировано в 1-й линии: <strong>" . User::getTurnoverByLine($user, 1) . " USD</strong>
						 Инвестировано в 2-й линии: <strong>" . User::getTurnoverByLine($user, 2) . " USD</strong>			 
						 Инвестировано в 3-й линии: <strong>" . User::getTurnoverByLine($user, 3) . " USD</strong>
						 
						 Заработано с 1-й линии: <strong>" . User::getProfitByLine($user, 1) . " USD</strong>
						 Заработано с 2-й линии: <strong>" . User::getProfitByLine($user, 2) . " USD</strong>				 
						 Заработано с 3-й линии: <strong>" . User::getProfitByLine($user, 3) . " USD</strong>

						 Ваш Telegram ID: <strong>$chat_id</strong>
						  
						 Выберите дальнейшее действие:
						 ";
                    $keyboard = [[hex2bin('F09F9497') . " Реферальная ссылка", hex2bin('F09F91A4') . " Мой пригласитель"], [hex2bin('F09F91A5') . " Моя команда", hex2bin('F09F8FA0') . " Главное меню"], [hex2bin('f09f938a') . " Баннеры"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

                } else {

                    $reply = "<strong>Ошибка! Неавторизированное действие. Пользователь: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Ошибка! Неавторизированное действие. Повторите вход в систему или зарегистрируйтесь</strong> \n";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                }
            } elseif ($text == hex2bin('F09F91A5') . " Моя команда") {

                $i = 1;
                $list = '';
                $user = $this->getUser($chat_id, $telegram);

                if ($user) {
                    /*Log::debug('TelegramController.child', [
                        'update' => $user
                    ]);*/
                    foreach ($user->users as $child) {

                        $list .= $i . '. <strong>' . $child->name . '</strong>';
                        $list .= "\n";
                        $i++;
                    }
                    $reply = $list . "\n\nВыберите дальнейшее действие";
                    $keyboard = [["1-я линия", "2-я линия", "3-я линия"], [hex2bin('F09F8FA0') . " Главное меню"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $reply = "<strong>Ошибка! Неавторизированное действие. Пользователь: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Ошибка! Неавторизированное действие. Повторите вход в систему или зарегистрируйтесь</strong> \n";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }

            } elseif ($text == hex2bin('F09F94A8') . " Настройки") {
                $reply = "Здесь вы можете добавить или изменить свои кошельки.";
                $keyboard = [["Изменить AdvCash кошелек", "Изменить Perfect кошелек"], ["Изменить BTC кошелек", "Изменить ETH кошелек"], [hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                // $telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            } elseif ($text == hex2bin('F09F92B0') . " Мои депозиты") {

                $i = 1;
                $list = "<strong>Ваши депозиты:</strong>\n\n";
                $user = $this->getUser($chat_id, $telegram);
                foreach ($user->balances as $balance) {
                    $list .= $i . '. Сумма: <strong>' . $balance->amount . '</strong> Валюта:<strong>' . $balance->currency . '</strong> Дата: <strong>' . $balance->created_at->format("d/m/Y H:i") . '</strong>';
                    $list .= "\n";
                    $i++;
                }
                if (!isset($balance)) $list .= "У Вас нет размешенных депозитов";
                $reply = $list . "\n\nВыберите дальнейшее действие";
                $keyboard = [[hex2bin('F09F92B5') . " Создать депозит", hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $telegram->sendMessage(['chat_id' => $chat_id, 'parse_mode' => 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup]);

            } elseif ($text == hex2bin('F09F938A') . " Операции") {

                $i = 1;
                $list = "<strong>Операции [5 последних]:</strong>\n\n";
                $user = $this->getUser($chat_id, $telegram);
                foreach (BalanceHistory::where('user_id', $user->id)->take(5)->OrderBy('id', 'desc')->get() as $balance) {
                    $list .= $i . '. Сумма: <strong>' . $balance->amount . '</strong> Валюта:<strong>' . $balance->currency . '</strong> Дата: <strong>' . $balance->created_at->format("d/m/Y H:i") . '</strong>' . "\n" . 'Источник: <strong>' . $balance->source . '</strong> ' . "\n" . 'Описание: <strong>' . $balance->description . '</strong>';
                    $list .= "\n\n";
                    $i++;
                }
                if ($i == 1) $list .= "У Вас нет проведенных транзакций";
                $reply = $list . "\n\nВыберите дальнейшее действие";
                if ($user->internal_balances()->where('currency', 'USD')->first()) {
                    $keyboard = [[hex2bin('F09F92B0') . " Мои депозиты", hex2bin('F09F92B8') . " Вывести", hex2bin('F09F92B2') . " Реинвест"], [hex2bin('F09F938A') . " Операции", hex2bin('F09F939D') . " Заявки на вывод"], [hex2bin('F09F8FA0') . " Главное меню"]];
                } else {
                    $keyboard = [[hex2bin('F09F92B0') . " Мои депозиты"], [hex2bin('F09F938A') . " Операции", hex2bin('F09F939D') . " Заявки на вывод"], [hex2bin('F09F8FA0') . " Главное меню"]];
                }

                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            } elseif ($text == hex2bin('F09F92B5') . " Создать депозит") {
                $reply = "Выберите валюту";
                $keyboard = [["Доллар США"], [hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            } elseif ($text == "Изменить AdvCash кошелек") {
                $user = User::where('telegram_id', $chat_id)->first();
                $wallet = $user->wallets()->where('title', 'AdvCash')->first();
                if ($wallet) {
                    $reply = "Текущий Кошелек <b>$wallet->number</b>";
                } else {
                    $reply = "<strong>Добавить AdvCash-кошелек</strong>";
                }
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                $this->setCommand($chat_id, "set_advcash");
            } elseif ($text == "Изменить Perfect кошелек") {
                $user = User::where('telegram_id', $chat_id)->first();
                $wallet = $user->wallets()->where('title', 'PerfectMoney')->first();
                if ($wallet) {
                    $reply = "Текущий Кошелек <b>$wallet->number</b>";
                } else {
                    $reply = "<strong>Добавить Perfect Money кошелек</strong>";
                }
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                $this->setCommand($chat_id, "set_perfect");
            } elseif ($text == "Изменить BTC кошелек") {
                $user = User::where('telegram_id', $chat_id)->first();
                $wallet = $user->wallets()->where('title', 'Bitcoin')->first();
                if ($wallet) {
                    $reply = "Текущий Кошелек <b>$wallet->number</b>";
                } else {
                    $reply = "<strong>Добавить BTC-кошелек</strong>";
                }
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                $this->setCommand($chat_id, "set_btc");
            } elseif ($text == "Изменить ETH кошелек") {
                $user = User::where('telegram_id', $chat_id)->first();
                $wallet = $user->wallets()->where('title', 'Ethereum')->first();
                if ($wallet) {
                    $reply = "Текущий Кошелек <b>$wallet->number</b>";
                } else {
                    $reply = "<strong>Добавить ETH-кошелек</strong>";
                }
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                $this->setCommand($chat_id, "set_eth");
            } elseif ($text == "Доллар США") {
                $reply = "Выберите платежную систему.";
                $keyboard = [["Perfect Money", "Advanced Cash"], [hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                //  $this->setCommand($chat_id, "invest_money");
            } elseif ($text == hex2bin('F09F9497') . " Реферальная ссылка") {
                $reply = "<strong>Ваша парнерская ссылка</strong>:\n <a href='tg://resolve?domain=FRSProjectBot&start=$chat_id'>https://telegram.me/FRSProjectBot?start=$chat_id</a>";
                $keyboard = [[hex2bin('F09F9497') . " Реферальная ссылка", hex2bin('F09F91A4') . " Мой пригласитель"], [hex2bin('F09F91A5') . " Моя команда", hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
            } elseif ($text == "Вывести с PerfectMoney") {
                $reply = "Введите сумму";
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $this->setCommand($chat_id, 'withdraw_from_perfect');
            } elseif ($text == "Вывести с AdvCash") {
                $reply = "Введите сумму";
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $this->setCommand($chat_id, 'withdraw_from_advcash');
            } elseif ($text == "Вывести с Ethereum") {
                $reply = "Введите сумму";
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $this->setCommand($chat_id, 'withdraw_from_ethereum');
            } elseif ($text == "Вывести с Bitcoin") {
                $reply = "Введите сумму";
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $this->setCommand($chat_id, 'withdraw_from_bitcoin');
            } elseif ($text == hex2bin('F09F92B8') . " Вывести") {
                $reply = "Выберите Кошелек";
                $wallets = [];
                $user = User::where('telegram_id', $chat_id)->first();
                if(Carbon::now()->endOfWeek()){
                    foreach ($user->wallets()->get() as $wallet) {
                        $wallets[] = ['Вывести с ' . $wallet->title];
                    }
                }else{
                    $reply = "Вывод доступен только по воскресеньям.";
                }
                $wallets[] = [hex2bin('F09F8FA0') . " Главное меню"];
                $keyboard = $wallets;
                //Log::info($wallets);
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
            }  elseif ($text == hex2bin('F09F94A8') . " Настройки") {
                $reply = "Свежие новости";
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $news = News::orderBy('id', 'desc')->take(5)->get();
                foreach ($news as $one_news) {
                    $reply = "<b>" . $one_news->title . "</b>\n\n" . $one_news->text;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                }
            } elseif ($text == hex2bin('F09F939D') . " Заявки на вывод") {
                $reply = "Заявки на вывод обрабатываются в порядке очереди по времени поступления. \n\n";
                $user = User::where('telegram_id', $chat_id)->first();
                foreach ($user->withdrawal_applications as $wa) {
                    $reply .= "Завка номер <b>" . $wa->id . "</b>.\n Вывод на кошелек " . $wa->wallet_number
                        . "\n Cумма <b>" . $wa->amount . " " . $wa->currency . "</b>. \n"
                        . "Статус : <b>" . $wa->getStatus() . "</b>\n\n";

                }
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
            } elseif ($text == "1-я линия") {
                $i = 1;

                $list = "<b>Ваша 1-я линия.</b> \n \n";
                $user = $this->getUser($chat_id, $telegram);


                if ($user) {

                    foreach (User::getChildrenByLine($user, 1) as $user) {
                        $list .= $i . '. <strong>' . $user->name . '</strong>';

                        $list .= "\n";
                        $i++;
                    }

                    $reply = $list . "\n\nВыберите дальнейшее действие";

                    $keyboard = [["1-я линия", "2-я линия", "3-я линия"], [hex2bin('F09F8FA0') . " Главное меню"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                } else {
                    $reply = "<strong>Ошибка! Неавторизированное действие. Пользователь: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Ошибка! Неавторизированное действие. Повторите вход в систему или зарегистрируйтесь</strong> \n";
                    $keyboard = [["/start"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                }
            } elseif ($text == "2-я линия") {
                $i = 1;

                $list = "<b>Ваша 2-я линия.</b> \n \n";
                $user = $this->getUser($chat_id, $telegram);


                if ($user) {

                    foreach (User::getChildrenByLine($user, 2) as $user) {
                        $list .= $i . '. <strong>' . $user->name . '</strong>';

                        $list .= "\n";
                        $i++;
                    }

                    $reply = $list . "\n\nВыберите дальнейшее действие";

                    $keyboard = [["1-я линия", "2-я линия", "3-я линия"], [hex2bin('F09F8FA0') . " Главное меню"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                } else {
                    $reply = "<strong>Ошибка! Неавторизированное действие. Пользователь: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Ошибка! Неавторизированное действие. Повторите вход в систему или зарегистрируйтесь</strong> \n";
                    $keyboard = [["/start"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                }
            } elseif ($text == "3-я линия") {
                $i = 1;

                $list = "<b>Ваша 3-я линия.</b> \n \n";
                $user = $this->getUser($chat_id, $telegram);


                if ($user) {

                    foreach (User::getChildrenByLine($user, 3) as $user) {
                        $list .= $i . '. <strong>' . $user->name . '</strong>';

                        $list .= "\n";
                        $i++;
                    }

                    $reply = $list . "\n\nВыберите дальнейшее действие";

                    $keyboard = [["1-я линия", "2-я линия", "3-я линия"], [hex2bin('F09F8FA0') . " Главное меню"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                } else {
                    $reply = "<strong>Ошибка! Неавторизированное действие. Пользователь: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Ошибка! Неавторизированное действие. Повторите вход в систему или зарегистрируйтесь</strong> \n";
                    $keyboard = [["/start"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }
            } elseif ($text == hex2bin('F09F92B2') . " Реинвест") {
                $user = User::where('telegram_id', $chat_id)->first();
                $reply = "Вы можете ввести сумму в текстовое поле не меньше <strong>10$</strong>. \n Или выбрать из предложеных вариантов реинвеста.";
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"], [hex2bin('F09F92B0') . " Мои депозиты"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);


                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'disable_web_page_preview' => true, 'text' => $reply, 'reply_markup' => $reply_markup ]);

                $inlineLayout = [[
                    Keyboard::inlineButton(['text' => 'Все', 'callback_data' => "reinvest_all"]),
                ],
                    [
                        Keyboard::inlineButton(['text' => '50$', 'callback_data' => "reinvest_50"]),
                        Keyboard::inlineButton(['text' => '100$', 'callback_data' => "reinvest_100"]),
                        Keyboard::inlineButton(['text' => '150$', 'callback_data' => "reinvest_150"]),
                        Keyboard::inlineButton(['text' => '200$', 'callback_data' => "reinvrst_200"]),
                    ], [
                        Keyboard::inlineButton(['text' => '300$', 'callback_data' => "reinvest_300"]),
                        Keyboard::inlineButton(['text' => '400$', 'callback_data' => "reinvest_400"]),
                        Keyboard::inlineButton(['text' => '500$', 'callback_data' => "reinvest_500"]),
                        Keyboard::inlineButton(['text' => '600$', 'callback_data' => "reinvest_600"]),
                    ], [
                        Keyboard::inlineButton(['text' => '700$', 'callback_data' => "reinvest_700"]),
                        Keyboard::inlineButton(['text' => '800$', 'callback_data' => "reinvest_800"]),
                        Keyboard::inlineButton(['text' => '900$', 'callback_data' => "reinvest_900"]),
                        Keyboard::inlineButton(['text' => '1000$', 'callback_data' => "reinvest_1000"]),
                    ]
                ];

                $inline_keyboard = Telegram::replyKeyboardMarkup([
                    'inline_keyboard' => $inlineLayout,
                ]);

                if ($int_balance = $user->internal_balances()->where('currency', 'USD')->first())
                    $balance = $int_balance->amount;
                else
                    $balance = 0.0000;
                $reply = "Доступный баланс <b>" . round($balance, 2) . "$</b>. \n Выберите сколько хотите реинвестировать.";
                $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $inline_keyboard ]);

                $this->setCommand($chat_id, "reinvest_money");

            } elseif ($text == hex2bin('F09F998B') . " Поддержка") {
                $reply = "Для соединения с оператором нажмите кнопку <strong>\"Начать чат\"</strong>";
                    $inlineLayout = [[
                        Keyboard::inlineButton(['text' => 'Начать чат', 'url' => "https://t.me/frsproject2018"]),
                    ]];
                    $inline_keyboard = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
                    $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);

            } elseif ($text == hex2bin('E29C8F') . " Общий чат") {
                $reply = "Для перехода в общий чат нажмите кнопку <strong>\"Начать чат\"</strong>";
                    $inlineLayout = [[
                        Keyboard::inlineButton(['text' => 'Начать чат', 'url' => "https://t.me/joinchat/Hc_M2EytQXtWEHkkY2MFaA"]),
                    ]];
                    $inline_keyboard = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
                    $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);

            }  elseif ($text == hex2bin('F09F93B0') . " Новости") {
				$news = News::take(5)->get();
				
				$list = '';
				foreach ($news as $record) {

                        $list .= '['.$record->created_at->format('d/m/Y').'] <strong>' . $record->title . '</strong>';
                        $list .= "\n";
                        $list .= $record->text;
                        $list .= "\n";
                    }
                $reply = $list;
                    $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
					
					
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            }  elseif ($text == hex2bin('e29d93') . " Вопрос\Ответ") {
                $reply = "
				<strong>Что такое FRS Project?</strong>
FRS Project- Функциональная резервная система. Это система трейдинга с созданием резервного капитала на каждую сделку единоразово.
<strong>Какие способы инвестирования существуют?</strong>
Bitcoin, Advcash, Ripple, Perfect Money, Ethereum.
<strong>Могу ли я добавить к уже созданному депозиту некоторую сумму и таким образом увеличить номинал инвестиции?</strong>
Да, это возможно.
<strong>Что такое партнерская программа?</strong>
Это программа, которая предусматривает начисление и выплату партнерского вознаграждения пользователям, чьи приглашенные партнеры создали инвестицию
<strong>Каковы условия начисления партнерского бонуса?</strong>
Ваш партнер должен указать ваш username
 В разделе (мой пригласитель) 
<strong>Сколько реферальных линий и какой процент прибили?</strong>
Всего 3 реферальные линии с доходностью.
🥇За привлечение партнера в доверительное управление - 3% от депозита и 10% от полученной прибыли
🥈За привлечение партнера в доверительное управление 2 линия - 2.5% от депозита и 7% от полученной прибыли
🥉За привлечение партнера в доверительное управление 3 линия - 2% от депозита и 5% от полученной прибыли.
<strong>Как часто происходит вывод процентов либо тела депозита?</strong>
Вывод происходит каждое воскресенье по согласованию.

				";
                    $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
					
					
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            }   elseif ($text == hex2bin('f09f938c') . " О нас") {
                $reply = "
				О ПРОЕКТЕ..
FRS-Functional Reserve System», проект с функциональной резервной системой капитала.
Основным направлением проекта является трейдинг криптовалют. На данный момент «FRS» это стабильно развивающийся проект, главный актив — это наши клиенты. Мы заинтересованы в развитии своих клиентов и росте их благосостояния, мы выстраиваем с людьми партнерские взаимоотношения, и эффективность нашей совместной работы повышается за счет этой синергии. Наша цель – построение долгосрочных партнерских отношений и устойчивого совместного бизнеса. Направление криптовалют безусловно новые, еще 7 лет назад никто не слышал о таком активе. Рынок криптовалют очень рискован и не терпит ошибок, так же можно сказать, что это рынок первобытный, «FRS» устойчива к любым изменениям на рынке, а статисты и финансовые аналитики регулярно следят за изменениями, мы видели много взлетов и падений как криптовалют, так и таких активов как нефть либо акции. В случае форс-мажорных обстоятельств сразу же принимаются в ход стратегии для ликвидации риска того или иного сектора.
				";
                    $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
					
					
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            } elseif ($text == hex2bin('F09F8FA0') . " Главное меню") {
                $user = $this->getUser($chat_id, $telegram);

                if ($user) {
                    $this->clearCommand($user);
                    $reply = "Выберите действие";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $reply = "<strong>Ошибка! Неавторизированное действие. Пользователь: $chat_id</strong>\n";
                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Ошибка! Неавторизированное действие. Повторите вход в систему или зарегистрируйтесь</strong> \n";
                    $keyboard = [["/start"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }


            } elseif ($text == hex2bin('F09F9396') . " Маркетинг") {

                $this->clearCommand($user);
                $reply = "⚡️Маркетинг⚡️ 
   Депозит возвратный, минимальный период от 1 месяца до 12 месяцев.
   💸 Инвестиции до 5000 $ прибыль делится 50/50 (между партнером и доверительным управлением )
 💸Средний дневной процент от 0.8 до 4% в день
💸   Суммы, которые выше 5000 $ ( Условия обговариваются лично ) 
💎 Минимальный депозит от 10$
💎 Минимальный вывод 1$
Начисления процентов обновляются на балансе каждый день в нашем телеграмм бот.
💵Вывод каждое воскресенье. 🏦💰💵
📢Партнерские бонусы FRS.
🥇1 линия -  3% (10%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
🥈2 линия - 2.5%(7%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
🥉3 линия - 2%(5%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
Начисления каждый день.
Вывод каждое воскресенье.
Доступные способы ВВОДА:  Bitcoin, ADVcash, Perfect Money, Ethereum";
                $keyboard = $mainMenu;
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            } elseif ($text == "Установить пригласителя") {
                $reply = "📢Партнерские бонусы FRS.
🥇1 линия -  3% (10%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
🥈2 линия - 2.5%(7%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
🥉3 линия - 2%(5%) -  \"За привлечение партнера в доверительное управление (от полученной прибыли)\"
Начисления каждый день.
Вывод каждое воскресенье.
Доступные способы ВВОДА:  Bitcoin, ADVcash, Perfect Money, Ethereum

		Отправьте ник того, кто пригласил Вас (вида @FRSProjectBot)";

                $keyboard = [["Установить пригласителя позже"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

                $this->setCommand($chat_id, "set_refer");
            } elseif ($text == "Perfect Money") {
				$this->setCommand($chat_id, "perfect-money");
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"], [hex2bin('F09F92B0') . " Мои депозиты"]];
                $reply = "Вы можете ввести сумму в текстовом поле не меньше 10$. \n Или выбрать из предложенных.";
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                $inlineLayout = [
                    [
                        Keyboard::inlineButton(['text' => '50$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/50"]),
                        Keyboard::inlineButton(['text' => '100$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/100"]),
                        Keyboard::inlineButton(['text' => '150$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/150"]),
                        Keyboard::inlineButton(['text' => '200$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/200"]),
                    ], [
                        Keyboard::inlineButton(['text' => '300$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/300"]),
                        Keyboard::inlineButton(['text' => '400$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/400"]),
                        Keyboard::inlineButton(['text' => '500$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/500"]),
                        Keyboard::inlineButton(['text' => '600$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/600"]),
                    ], [
                        Keyboard::inlineButton(['text' => '700$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/700"]),
                        Keyboard::inlineButton(['text' => '800$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/800"]),
                        Keyboard::inlineButton(['text' => '900$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/900"]),
                        Keyboard::inlineButton(['text' => '1000$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/1000"]),
                    ]
                ];

                $inline_keyboard = Telegram::replyKeyboardMarkup([
                    'inline_keyboard' => $inlineLayout,
                ]);

                $reply = "Выберите сумму пополнения";
                $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);
                //$this->setCommand($chat_id, "invest_money");
            } elseif ($text == "Advanced Cash") {
				$this->setCommand($chat_id, 'advcash');
                $keyboard = [[hex2bin('F09F8FA0') . " Главное меню"]];
                $reply = "Уважаемые партнеры, временно пополнения через \"Advanced Cash\" принимаем в ручном режиме, для пополнения, перейдите в чат с нашей администрацией.";
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
				$this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
				 $inlineLayout = [[
                        Keyboard::inlineButton(['text' => 'Начать чат', 'url' => "https://t.me/frsproject2018"]),
                    ]];
                    $inline_keyboard = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
				$reply = 'С Ув. FRS Project!';
                $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);
                //$this->setCommand($chat_id, "invest_money");
            } else {
                $reply = "По запросу \"<b>" . $text . "</b>\" ничего не найдено.";
                $this->sendMessage($telegram, $chat_id, $reply);

            }
        }
    }
}
