<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;
use App\User;
use App\News;
use App\InternalBalance;
use App\Setting;
use App\Wallet;
use App\Balance;
use App\BalanceHistory;
use App\WithdrawalApplication;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Carbon\Carbon;

class WebhookController extends Controller
{

    public function getStateCommand($chat_id)
    {
        if ($this->getCommandValue($chat_id)) {
            return true;
        }

        return false;
    }

    public function setReinvest($chat_id, $money, $telegram)
    {
        $user = User::where('telegram_id', $chat_id)->first();

        $int_balance = InternalBalance::where('user_id', $user->id)->where('currency', 'USD')->first();
// money_limit
        if ($int_balance->amount >= $money && $money >= 0.01) {
            $balance = new Balance([
                'user_id' => $user->id,
                'amount' => $money,
                'currency' => 'USD'
            ]);

            $balance->save();

            $int_balance->amount -= $money;
            $int_balance->save();

            $int_balance->balances()->attach($balance->id);


            $history = new BalanceHistory([
                'user_id' => $user->id,
                'amount' => $money,
                'currency' => 'USD',
                'source' => 'Reinvest. Account №' . $int_balance->id,
                'description' => 'Create a new deposit'
            ]);

            $balance->history()->save($history);

            $reply = "Congratulations! You have created a new deposit for the amount of<strong> $money USD</strong>";
            $keyboard = [[hex2bin('F09F92B0') . " My deposits", hex2bin('F09F92B8') . " Withdraw", hex2bin('F09F92B2') . " Reinvest"], [hex2bin('F09F938A') . " Operations", hex2bin('F09F939D') . " Applications for withdrawal"], [hex2bin('F09F8FA0') . " Main menu"]];
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            $this->clearCommand(User::where('telegram_id', $chat_id)->first()->name);

        } else {
            $reply = "Not enough money to commit operations.";
            $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            $this->clearCommand(User::where('telegram_id', $chat_id)->first()->name);
        }
    }

    public function getCommandValue($chat_id)
    {
        if ($user = User::where("telegram_id", $chat_id)->first()) {
            $pivot = $user->settings()->where("setting", "command")->first()->pivot;
            if ($value = $pivot->value) {
                return $value;
            }
        }

        return false;
    }

    public function clearCommand($chat_id)
    {
        if ($user = User::where("telegram_id", $chat_id)->first()) {
            $pivot = $user->settings()->where("setting", "command")->first()->pivot;
            $pivot->value = '';
            $pivot->save();
        }
    }

    public function setCommand($chat_id, $command)
    {
        if ($user = User::where("telegram_id", $chat_id)->first()) {
            Log::info($user);
            $pivot = $user->settings()->where("setting", "command")->first()->pivot;
            $pivot->value = $command;
            $pivot->save();

        } else return false;
    }

    public function getUser($chat_id, $telegram)
    {

        if ($user = User::where('telegram_id', $chat_id)->first()) {
            Log::debug('TelegramController.getUser', [
                'action' => 'Запрос на авторизацию получен: ' . $chat_id
            ]);
            return $user;
        } else {

            return null;
        }
    }

    public function sendMessage($telegram, $chat_id, $reply, $reply_markup = null, $disable_web_page_preview = null)
    {

        try {

            $telegram->sendMessage([
                'chat_id' => $chat_id,
                'text' => $reply,
                'parse_mode' => 'HTML',
                'reply_markup' => $reply_markup ?: false,
                'disable_web_page_preview' => $disable_web_page_preview ?: false,
            ]);

        } catch (TelegramResponseException $e) {

            $errorData = $e->getResponseData();

            if ($errorData['ok'] === false) {
                $telegram->sendMessage([
                    'chat_id' => env('TELEGRAM_ADMIN_ID'),
                    'text' => 'There was an error for a user. ' . $errorData['error_code'] . ' ' . $errorData['description'],
                ]);
            }
        }

    }

    public function withdrawMoney($chat_id, $money, $telegram, $wallet)
    {
        $user = User::where('telegram_id', $chat_id)->first();

        $int_balance = InternalBalance::where('user_id', $user->id)->where('currency', 'USD')->first();

        if ($int_balance->amount >= $money) {
            $withdrawal_app = new WithdrawalApplication([
                'user_id' => $user->id,
                'internal_balance_id' => $int_balance->id,
                'currency' => $wallet->currency,
                'wallet_number' => $wallet->title . ' ' . $wallet->number,
                'amount' => $money,
                'status' => 'new',
                'is_completed' => null
            ]);

            $withdrawal_app->save();

            $int_balance->amount -= $money;
            $int_balance->save();

            // $int_balance->withdrawal_app()->attach($withdrawal_app->id);


            $history = new BalanceHistory([
                'user_id' => $user->id,
                'amount' => $money,
                'currency' => 'USD',
                'source' => 'Balance',
                'description' => 'Blocking of funds for an application for withdrawal. Application number №' . $int_balance->id,
            ]);

            //$withdrawal_app->history()->save($history);

            $reply = "Congratulations! You applied for an output <strong> $money USD</strong>";
            $keyboard = [[hex2bin('F09F92B0') . " My deposits", hex2bin('F09F92B8') . " Withdraw", hex2bin('F09F92B2') . " Reinvest"], [hex2bin('F09F938A') . " Operations", hex2bin('F09F939D') . " Applications for withdrawal"], [hex2bin('F09F8FA0') . " Main menu"]];
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            $this->clearCommand(User::where('telegram_id', $chat_id)->first()->name);

        } else {
            $reply = "Not enough money to commit operations.";
            $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            $this->clearCommand(User::where('telegram_id', $chat_id)->first()->name);
        }
    }


    public function index()
    {

        $telegram = new Api(env('TELEGRAM_BOT_TOKEN')); //Устанавливаем токен, полученный у BotFather
        $result = $telegram->getWebhookUpdates(); //Передаем в переменную $result полную информацию о сообщении пользователя

        Log::debug('TelegramController.process', [
            'update' => $result
        ]);

        $message = $result->getMessage();

        $callback = $result->getCallbackQuery();
        $edited = $result->getEditedMessage();

        $mainMenu = [[hex2bin('F09F92B5') .  " Finance", hex2bin('F09F91AA') . " Partners"], [hex2bin('F09F92B0') . " My deposits", hex2bin('F09F94A8') . " Settings"], [hex2bin('F09F93B0') . " News", hex2bin('E29C8F') . " Shared chat"],[hex2bin('f09f938c') . " About us", hex2bin('e29d93') . " FAQ"], [hex2bin('F09F9396') . " Marketing", hex2bin('F09F998B') . " Support"]];

        if ($message) {


            $user = $message->getFrom();
            $chat_id = $user->getId();
            $text = $message->getText();
            $name = $user->getUsername() ?: '';
            $last_name = $user->getLastName() ?: '';
            $first_name = $user->getFirstName() ?: '';
            $full_name = $first_name . ' ' . $last_name;
      
/*
		 $reply = "В данный момент сервис находится на техническом обслуживании. \n
Приносим извинения за временные неудобства. \n
Спасибо за понимание.";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    return 'ok';*/




  } elseif ($callback) {
            $message = $callback->getData();
            if ($message == 'back') {
                $reply = "Choose a further action \n";
                $keyboard = $mainMenu;
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $callback->getFrom()->getId(), $reply, $reply_markup);
                //$telegram->sendMessage([ 'chat_id' => $callback->getFrom()->getId(), 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
            } elseif ($message == 'reinvest_50') {
                $this->setReinvest($callback->getFrom()->getId(), 50, $telegram);
            } elseif ($message == 'reinvest_100') {
                $this->setReinvest($callback->getFrom()->getId(), 100, $telegram);
            } elseif ($message == 'reinvest_150') {
                $this->setReinvest($callback->getFrom()->getId(), 150, $telegram);
            } elseif ($message == 'reinvest_200') {
                $this->setReinvest($callback->getFrom()->getId(), 200, $telegram);
            } elseif ($message == 'reinvest_300') {
                $this->setReinvest($callback->getFrom()->getId(), 300, $telegram);
            } elseif ($message == 'reinvest_400') {
                $this->setReinvest($callback->getFrom()->getId(), 400, $telegram);
            } elseif ($message == 'reinvest_500') {
                $this->setReinvest($callback->getFrom()->getId(), 500, $telegram);
            } elseif ($message == 'reinvest_600') {
                $this->setReinvest($callback->getFrom()->getId(), 600, $telegram);
            } elseif ($message == 'reinvest_700') {
                $this->setReinvest($callback->getFrom()->getId(), 700, $telegram);
            } elseif ($message == 'reinvest_800') {
                $this->setReinvest($callback->getFrom()->getId(), 800, $telegram);
            } elseif ($message == 'reinvest_900') {
                $this->setReinvest($callback->getFrom()->getId(), 900, $telegram);
            } elseif ($message == 'reinvest_1000') {
                $this->setReinvest($callback->getFrom()->getId(), 1000, $telegram);
            } elseif ($message == 'reinvest_all') {
                $user = User::where('telegram_id', $callback->getFrom()->getId())->first();
                $int_balance = InternalBalance::where('user_id', $user->id)->where('currency', 'USD')->first();
                if ($int_balance->amount > 0) {
                    $this->setReinvest($callback->getFrom()->getId(), $int_balance->amount, $telegram);
                }
            }
            return 'ok'; //if callback query
        } else {
            return 'ok'; //if edited_message
        }

        if (starts_with($text, '/start') && strlen($text) > 6) {

            list($str, $referer_id) = explode(" ", $text);

            $user = $this->getUser($chat_id, $telegram);

            if (!$user) {
                $setting = Setting::where("setting", "command")->first();
                if ($name) {
                    $user = new User();
                    $user->email = $chat_id . "@frsproject.com";
                    $user->telegram_id = $chat_id;
                    $user->name = "@" . $name;
                    $user->full_name = $full_name;
                    $user->password = bcrypt('123456');
                    $user->save();
                    $user->settings()->attach($setting->id);
                    $reply = "Congratulations on your registration in <strong>FRS Project</strong>. \n";
                } else {
                    $reply = "Error! Only users with a filled-in <strong>username</strong> field can be registered.
								 Please, fill out the appropriate field in your account settings and repeat the registration.";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                    return 'ok';
                }
            }


            $referer = $this->getUser($referer_id, $telegram);

            Log::info('Строка 129 ' . $referer);

            if (!$referer) {

                $reply = "A user with the number <strong> $ referer_id </ strong> was not found.\nYou can set up an inviter in the <strong> Partners </ strong> => <strong> My Inviter </ strong> => <strong> Set up an Inviter </ strong>";

            }

            if ($referer && $user) {


                Log::info('Юзер для закрепления реферера найден');

                if ($user->id == $referer->id) {
                    Log::info('Юзер устанавливает сам себя');
                    $reply = "It is forbidden to set up your own account as an inviter";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    return 'ok';
                }

                if($user->refererIsValid($referer->id)){
                    $user->parent_id = $referer->id;
                    $user->save();
                    $reply = "Your inviter is installed as<strong>$referer->name ( $referer_id )</strong>.\nChoose a further action";

                }else{
                    Log::info('Юзер устанавливает себе родителя из цепочки.');
                    $reply = "You can not set this user as an inviter.";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    return 'ok';
                }

            }

            $keyboard = $mainMenu;
            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
            $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            return 'ok';
        }

        if ($name && $this->getStateCommand($chat_id)) {
            if ($this->getCommandValue($chat_id) == 'set_refer') {
                if ($refer = User::where('name', $text)->first() && $user = $this->getUser($chat_id, $telegram)) {
                    $refer = User::where('name', $text)->first();
                    if ($user->id != $refer->id and $user->refererIsValid($refer->id)){
                        $user->parent_id = $refer->id;
                        $reply = "Your inviter is installed as: " . $text;
                    }else{
                        $reply = "You can not set this user as an inviter.";
                    }
                    $user->save();

                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                    $this->clearCommand($chat_id);
                } elseif ($text == "Install inviter later") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action \n";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $reply = "The user with the nickname as <b>\"" . $text . "\"</b> not found.";

                    $this->sendMessage($telegram, $chat_id, $reply);
                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply ]);
                }
            } elseif ($this->getCommandValue($chat_id) == 'perfect-money' OR $this->getCommandValue($chat_id) == 'advcash') {
                if ($text == hex2bin('F09F8FA0') . " Main menu" OR $text == hex2bin('F09F92B0') . " My deposits") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    //if (is_numeric($text) and $text >= 10)
                    if ($text >= 0.01)
                        $sum = $text;
                    else
                        $sum = 100;
					$payment = $this->getCommandValue($chat_id);
                    $reply = "To confirm the refill for the amount of <strong>\"$sum$\"</strong> click the <b>\"Confirm\"</b> button";
                    $inlineLayout = [[
                        Keyboard::inlineButton(['text' => 'Confirm', 'url' => "https://frs-project.com/replenish/$payment/$chat_id/$sum"]),
                    ]];
                    $inline_keyboard = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
                    $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);
                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $inline_keyboard ]);

                    $this->clearCommand($chat_id);
                }
            } elseif ($this->getCommandValue($chat_id) == 'reinvest_money') {
                if ($text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    //if (is_numeric($text) and $text >= 10)
                    if ($text >= 0.01)
                        $sum = $text;
                    else
                        $sum = 100;
                    $this->setReinvest($chat_id, $sum, $telegram);
                }
            } elseif ($this->getCommandValue($chat_id) == 'withdraw_from_perfect') {
                if ($text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $wallet = User::where('telegram_id', $chat_id)->first()->wallets()->where('title', 'PerfectMoney')->first();
                    $this->withdrawMoney($chat_id, $text, $telegram, $wallet);
                }
            } elseif ($this->getCommandValue($chat_id) == 'withdraw_from_advcash') {
                if ($text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $wallet = User::where('telegram_id', $chat_id)->first()->wallets()->where('title', 'AdvCash')->first();
                    $this->withdrawMoney($chat_id, $text, $telegram, $wallet);
                }
            } elseif ($this->getCommandValue($chat_id) == 'withdraw_from_ethereum') {
                if ($text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $wallet = User::where('telegram_id', $chat_id)->first()->wallets()->where('title', 'Ethereum')->first();
                    $this->withdrawMoney($chat_id, $text, $telegram, $wallet);
                }
            } elseif ($this->getCommandValue($chat_id) == 'withdraw_from_bitcoin') {
                if ($text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $wallet = User::where('telegram_id', $chat_id)->first()->wallets()->where('title', 'Bitcoin')->first();
                    $this->withdrawMoney($chat_id, $text, $telegram, $wallet);
                }
            } elseif ($this->getCommandValue($chat_id) == 'set_advcash') {
                if ($text == "Отмена" or $text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {
                    $user = User::where('telegram_id', $chat_id)->first();
                    $wallet = $user->wallets()->where('title', 'AdvCash')->first();
                    if ($wallet) {
                        $wallet->number = $text;
                        $wallet->save();
                    } else {
                        $wallet = new Wallet([
                            'user_id' => $user->id,
                            'title' => 'AdvCash',
                            'currency' => 'USD',
                            'number' => $text
                        ]);
                        $wallet->save();
                    }
                    $reply = "You entered " . $text;
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    $this->clearCommand($chat_id);
                }
            } elseif ($this->getCommandValue($chat_id) == 'set_perfect') {
                if ($text == "Cancel" or $text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {

                    $user = User::where('telegram_id', $chat_id)->first();
                    $wallet = $user->wallets()->where('title', 'PerfectMoney')->first();
                    if ($wallet) {
                        $wallet->number = $text;
                        $wallet->save();
                    } else {
                        $wallet = new Wallet([
                            'user_id' => $user->id,
                            'title' => 'PerfectMoney',
                            'currency' => 'USD',
                            'number' => $text
                        ]);
                        $wallet->save();
                    }
                    $reply = "You entered " . $text;
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    $this->clearCommand($chat_id);
                }
            } elseif ($this->getCommandValue($chat_id) == 'set_btc') {
                if ($text == "Cancel" or $text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {

                    $user = User::where('telegram_id', $chat_id)->first();
                    $wallet = $user->wallets()->where('title', 'Bitcoin')->first();
                    if ($wallet) {
                        $wallet->number = $text;
                        $wallet->save();
                    } else {
                        $wallet = new Wallet([
                            'user_id' => $user->id,
                            'title' => 'Bitcoin',
                            'currency' => 'BTC',
                            'number' => $text
                        ]);
                        $wallet->save();
                    }
                    $reply = "You entered " . $text;
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    $this->clearCommand($chat_id);
                }
            } elseif ($this->getCommandValue($chat_id) == 'set_eth') {
                if ($text == "Cancel" or $text == hex2bin('F09F8FA0') . " Main menu") {
                    $this->clearCommand($chat_id);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                } else {

                    $user = User::where('telegram_id', $chat_id)->first();
                    $wallet = $user->wallets()->where('title', 'Ethereum')->first();
                    if ($wallet) {
                        $wallet->number = $text;
                        $wallet->save();
                    } else {
                        $wallet = new Wallet([
                            'user_id' => $user->id,
                            'title' => 'Ethereum',
                            'currency' => 'ETH',
                            'number' => $text
                        ]);
                        $wallet->save();
                    }
                    $reply = "You entered " . $text;
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    $this->clearCommand($chat_id);
                }
            }
        } elseif ($text) {
            if ($text == "/start") {
                if ($this->getUser($chat_id, $telegram)) {
                    $reply = "You are already registered in the system. \n Welcome back to <strong>FRS Project</strong>";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $setting = Setting::where("setting", "command")->first();
                    if ($name) {
                        $user = new User();
                        $user->email = $chat_id . "@frsproject.com";
                        $user->telegram_id = $chat_id;
                        $user->name = "@" . $name;
                        $user->full_name = $full_name;
                        $user->password = bcrypt('123456');
                        $user->save();
                        $user->settings()->attach($setting->id);
                        $reply = "Registration completed successfully. \n";

                        $reply .= "Hello! <b>$full_name</b>. Welcome back to FRS Project.
			zap:Реферальная система: Начисления каждый день, вывод каждые выходные
    🥇За привлечение партнера в доверительное управление - 3% от депозита и 10% от   полученной прибыли
    🥈За привлечение партнера в доверительное управление 2 линия - 2.5% от депозита и 7% от полученной прибыли
    🥉За привлечение партнера в доверительное управление 3 линия - 2% от депозита и 5% от полученной прибыли
    :bank:Доступные способы ввода инвестиция :dollar:
    :gem:Bitcoin, ADVcash, Perfect Money, Ethereum:moneybag:

			Send the nickname of the person who invited you (ex.: @FRSProject)";

                        $keyboard = [["Install inviter later"], [hex2bin('F09F92B5') .  " Finance", hex2bin('F09F94A8') . " Settings"], ["Banners", hex2bin('F09F94A8') . " Settings"]];
                        $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                        $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                        //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

                        $this->setCommand($chat_id, "set_refer");
                    } else {
                        $reply = "Error: Registration is allowed only for users with a <strong> username </ strong>.
								 Please fill out the appropriate field in your account settings and repeat the registration.";
                        $this->sendMessage($telegram, $chat_id, $reply);
                        return 'ok';

                        //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply ]);
                    }
                }
            } elseif ($text == hex2bin('F09F91A4') . " My Inviter") {
                Log::info($chat_id);
                Log::info($this->getUser($chat_id, $telegram));
                $user = $this->getUser($chat_id, $telegram);

                if ($user) {
                    if ($user->parent) {
                        $referer = $user->parent;
                        $referer_name = $referer->name;
                        $reply = "Your inviter is <strong>$referer_name</strong>\n Choose a further action";
                    } else {
                        $reply = "You do not have an inviter yet.";
                    }
                    $keyboard = [["Set a inviter"], [hex2bin('F09F8FA0') . " Main menu"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $reply = "<strong>Error! Unauthorized action. User: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Error! Unauthorized action. Log in or register again</strong> \n";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }

            } elseif ($text == hex2bin('F09F92B5') .  " Finance") {
                $user = $this->getUser($chat_id, $telegram);
                if ($user) {
                    $balance = $user->balances()->where('currency', 'USD')->first() ? $user->balances()->where('currency', 'USD')->sum('amount') : '0.00';
                    $int_balance = $user->internal_balances()->where('currency', 'USD')->first() ? $user->internal_balances()->where('currency', 'USD')->first()->amount : '0.00';
                    $reply = "
				<strong>Your Bank</strong>
				
				Your balance:
				<strong>USD - $int_balance $ </strong>\n
				The amount of your deposits:
				<strong>USD - $balance $</strong> \n
				Earned in the project:
				<strong>USD - 0.00 $</strong>";
                    if ($user->internal_balances()->where('currency', 'USD')->first()) {
                        $keyboard = [[hex2bin('F09F92B0') . " My deposits", hex2bin('F09F92B8') . " Withdraw"], [hex2bin('F09F938A') . " Operations", hex2bin('F09F939D') . " Applications for withdrawal"], [ hex2bin('F09F92B2') . " Reinvest", hex2bin('F09F8FA0') . " Main menu"]];
                    } else {
                        $keyboard = [[hex2bin('F09F92B0') . " My deposits"], [hex2bin('F09F938A') . " Operations", hex2bin('F09F939D') . " Applications for withdrawal"], [hex2bin('F09F8FA0') . " Main menu"]];
                    }
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

                } else {
                    $reply = "<strong>Error! Unauthorized action. User: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Error! Unauthorized action. Log in or register again</strong> \n";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }

            } elseif ($text == "Refill") {
                $reply = "Select a payment system";
                $keyboard = [["Perfect Money", "Advanced Cash"], [hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            } elseif ($text == hex2bin('F09F91AA') . " Partners") {
                $user = $this->getUser($chat_id, $telegram);
                if ($user) {
                    $reply = "<strong>Affiliate program</strong>\n
						 Total number of partners in your team: <strong>" . $user->users->count() . "</strong>
						 Active partners: <strong> " . User::getActiveChildrenCount($user) . " </strong>
						 
						 Total invested in your team: <strong>0.00 USD</strong>
						 The largest deposit: <strong>0.00 USD</strong>

						 Total number of partners in the 1-st line: <strong> " . User::getChildrensCountByLine($user, 1) . "  </strong>
						 Total number of partners in the 2-nd line: <strong> " . User::getChildrensCountByLine($user, 2) . " </strong>
						 Total number of partners in the 3-rd line: <strong> " . User::getChildrensCountByLine($user, 3) . " </strong>
						 
						 Invested in the 1-st line: <strong>" . User::getTurnoverByLine($user, 1) . " USD</strong>
						 Invested in the 2-nd line: <strong>" . User::getTurnoverByLine($user, 2) . " USD</strong>			 
						 Invested in the 3-rd line: <strong>" . User::getTurnoverByLine($user, 3) . " USD</strong>
						 
						 Earned from the 1-st line: <strong>0.00 USD</strong>
						 Earned from the 2-nd line: <strong>0.00 USD</strong>				 
						 Earned from the 3-rd line: <strong>0.00 USD</strong>

						 Your Telegram ID: <strong>$chat_id</strong>
						  
						 Choose a further action:
						 ";
                    $keyboard = [[hex2bin('F09F9497') . " Referral link", hex2bin('F09F91A4') . " My Inviter"], [hex2bin('F09F91A5') . " My team", hex2bin('F09F8FA0') . " Main menu"], [hex2bin('f09f938a') . " Banners"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

                } else {

                    $reply = "<strong>Error! Unauthorized action. User: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Error! Unauthorized action. Log in or register again</strong> \n";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                }
            } elseif ($text == hex2bin('F09F91A5') . " My team") {

                $i = 1;
                $list = '';
                $user = $this->getUser($chat_id, $telegram);

                if ($user) {
                    Log::debug('TelegramController.child', [
                        'update' => $user
                    ]);
                    foreach ($user->users as $child) {

                        $list .= $i . '. <strong>' . $child->name . '</strong>';
                        $list .= "\n";
                        $i++;
                    }
                    $reply = $list . "\n\nChoose a further action";
                    $keyboard = [["1-st line", "2-st line", "3-st line"], [hex2bin('F09F8FA0') . " Main menu"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $reply = "<strong>Error! Unauthorized action. User: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Error! Unauthorized action. Log in or register again</strong> \n";
                    $keyboard = [["/start"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);

                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }

            } elseif ($text == hex2bin('F09F94A8') . " Settings") {
                $reply = "Here you can add or change your wallets.";
                $keyboard = [["Change AdvCash wallet", "Change Perfect wallet"], ["Change BTC wallet", "Change ETH wallet"], [hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                // $telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            } elseif ($text == hex2bin('F09F92B0') . " My deposits") {

                $i = 1;
                $list = "<strong>Your deposits:</strong>\n\n";
                $user = $this->getUser($chat_id, $telegram);
                foreach ($user->balances as $balance) {
                    $list .= $i . '. Amount: <strong>' . $balance->amount . '</strong> Currency:<strong>' . $balance->currency . '</strong> Date: <strong>' . $balance->created_at->format("d/m/Y H:i") . '</strong>';
                    $list .= "\n";
                    $i++;
                }
                if (!isset($balance)) $list .= "You do not have any deposits placed";
                $reply = $list . "\n\nChoose a further action";
                $keyboard = [[hex2bin('F09F92B5') . " Create a deposit", hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $telegram->sendMessage(['chat_id' => $chat_id, 'parse_mode' => 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup]);

            } elseif ($text == hex2bin('F09F938A') . " Operations") {

                $i = 1;
                $list = "<strong>Operations:</strong>\n\n";
                $user = $this->getUser($chat_id, $telegram);
                foreach ($user->balance_histories as $balance) {
                    $list .= $i . '. Amount: <strong>' . $balance->amount . '</strong> Currency:<strong>' . $balance->currency . '</strong> Date: <strong>' . $balance->created_at->format("d/m/Y H:i") . '</strong>' . "\n" . 'Source: <strong>' . $balance->source . '</strong> ' . "\n" . 'Description: <strong>' . $balance->description . '</strong>';
                    $list .= "\n\n";
                    $i++;
                }
                if ($i == 1) $list .= "You do not have any transactions";
                $reply = $list . "\n\nChoose a further action";
                if ($user->internal_balances()->where('currency', 'USD')->first()) {
                    $keyboard = [[hex2bin('F09F92B0') . " My deposits", hex2bin('F09F92B8') . " Withdraw", hex2bin('F09F92B2') . " Reinvest"], [hex2bin('F09F938A') . " Operations", hex2bin('F09F939D') . " Applications for withdrawal"], [hex2bin('F09F8FA0') . " Main menu"]];
                } else {
                    $keyboard = [[hex2bin('F09F92B0') . " My deposits"], [hex2bin('F09F938A') . " Operations", hex2bin('F09F939D') . " Applications for withdrawal"], [hex2bin('F09F8FA0') . " Main menu"]];
                }

                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            } elseif ($text == hex2bin('F09F92B5') . " Create a deposit") {
                $reply = "Select currency";
                $keyboard = [["USD"], [hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

            } elseif ($text == "Change AdvCash wallet") {
                $user = User::where('telegram_id', $chat_id)->first();
                $wallet = $user->wallets()->where('title', 'AdvCash')->first();
                if ($wallet) {
                    $reply = "Current wallet <b>$wallet->number</b>";
                } else {
                    $reply = "<strong>Add AdvCash-wallet</strong>";
                }
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                $this->setCommand($chat_id, "set_advcash");
            } elseif ($text == "Change Perfect wallet") {
                $user = User::where('telegram_id', $chat_id)->first();
                $wallet = $user->wallets()->where('title', 'PerfectMoney')->first();
                if ($wallet) {
                    $reply = "Current wallet <b>$wallet->number</b>";
                } else {
                    $reply = "<strong>Add Perfect Money wallet</strong>";
                }
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                $this->setCommand($chat_id, "set_perfect");
            } elseif ($text == "Change BTC wallet") {
                $user = User::where('telegram_id', $chat_id)->first();
                $wallet = $user->wallets()->where('title', 'Bitcoin')->first();
                if ($wallet) {
                    $reply = "Current wallet <b>$wallet->number</b>";
                } else {
                    $reply = "<strong>Add BTC-wallet</strong>";
                }
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                $this->setCommand($chat_id, "set_btc");
            } elseif ($text == "Change ETH wallet") {
                $user = User::where('telegram_id', $chat_id)->first();
                $wallet = $user->wallets()->where('title', 'Ethereum')->first();
                if ($wallet) {
                    $reply = "Current wallet <b>$wallet->number</b>";
                } else {
                    $reply = "<strong>Add ETH-wallet</strong>";
                }
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                $this->setCommand($chat_id, "set_eth");
            } elseif ($text == "USD") {
                $reply = "Select a payment system.";
                $keyboard = [["Perfect Money", "Advanced Cash"], [hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                //  $this->setCommand($chat_id, "invest_money");
            } elseif ($text == hex2bin('F09F9497') . " Referral link") {
                $reply = "<strong>Your affiliate link</strong>:\n <a href='tg://resolve?domain=FRSProjectBot&start=$chat_id'>https://telegram.me/FRSProjectBot?start=$chat_id</a>";
                $keyboard = [[hex2bin('F09F9497') . " Referral link", hex2bin('F09F91A4') . " My Inviter"], [hex2bin('F09F91A5') . " My team", hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
            } elseif ($text == "Withdraw  from PerfectMoney") {
                $reply = "Enter amount";
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $this->setCommand($chat_id, 'withdraw_from_perfect');
            } elseif ($text == "Withdraw  from AdvCash") {
                $reply = "Enter amount";
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $this->setCommand($chat_id, 'withdraw_from_advcash');
            } elseif ($text == "Withdraw  from Ethereum") {
                $reply = "Enter amount";
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $this->setCommand($chat_id, 'withdraw_from_ethereum');
            } elseif ($text == "Withdraw  from Bitcoin") {
                $reply = "Enter amount";
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $this->setCommand($chat_id, 'withdraw_from_bitcoin');
            } elseif ($text == hex2bin('F09F92B8') . " Withdraw") {
                $reply = "Select wallet";
                $wallets = [];
                $user = User::where('telegram_id', $chat_id)->first();
                if(Carbon::now()->format('w') != 0){
                    foreach ($user->wallets()->get() as $wallet) {
                        $wallets[] = ['Withdraw  from ' . $wallet->title];
                    }
                }else{
                    $reply = "The withdraw is only available on Sundays.";
                }
                $wallets[] = [hex2bin('F09F8FA0') . " Main menu"];
                $keyboard = $wallets;
                Log::info($wallets);
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
            }  elseif ($text == hex2bin('F09F94A8') . " Settings") {
                $reply = "Latest news";
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                $news = News::orderBy('id', 'desc')->take(5)->get();
                foreach ($news as $one_news) {
                    $reply = "<b>" . $one_news->title . "</b>\n\n" . $one_news->text;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
                }
            } elseif ($text == hex2bin('F09F939D') . " Applications for withdrawal") {
                $reply = "Applications for withdrawal processed in the order of the queue by the time of receipt. \n\n";
                $user = User::where('telegram_id', $chat_id)->first();
                foreach ($user->withdrawal_applications as $wa) {
                    $reply .= "Application number <b>" . $wa->id . "</b>.\n Withdraw to wallet " . $wa->wallet_number
                        . "\n Amount <b>" . $wa->amount . " " . $wa->currency . "</b>. \n"
                        . "Status : <b>" . $wa->getStatus() . "</b>\n\n";

                }
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);
            } elseif ($text == "1-st line") {
                $i = 1;

                $list = "<b>Your 1-st line.</b> \n \n";
                $user = $this->getUser($chat_id, $telegram);


                if ($user) {

                    foreach (User::getChildrenByLine($user, 1) as $user) {
                        $list .= $i . '. <strong>' . $user->name . '</strong>';

                        $list .= "\n";
                        $i++;
                    }

                    $reply = $list . "\n\nChoose a further action";

                    $keyboard = [["1-st line", "2-st line", "3-st line"], [hex2bin('F09F8FA0') . " Main menu"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                } else {
                    $reply = "<strong>Error! Unauthorized action. User: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Error! Unauthorized action. Log in or register again</strong> \n";
                    $keyboard = [["/start"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                }
            } elseif ($text == "2-st line") {
                $i = 1;

                $list = "<b>Your 2-st line.</b> \n \n";
                $user = $this->getUser($chat_id, $telegram);


                if ($user) {

                    foreach (User::getChildrenByLine($user, 2) as $user) {
                        $list .= $i . '. <strong>' . $user->name . '</strong>';

                        $list .= "\n";
                        $i++;
                    }

                    $reply = $list . "\n\nChoose a further action";

                    $keyboard = [["1-st line", "2-st line", "3-st line"], [hex2bin('F09F8FA0') . " Main menu"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                } else {
                    $reply = "<strong>Error! Unauthorized action. User: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Error! Unauthorized action. Log in or register again</strong> \n";
                    $keyboard = [["/start"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                }
            } elseif ($text == "3-st line") {
                $i = 1;

                $list = "<b>Your 3-st line.</b> \n \n";
                $user = $this->getUser($chat_id, $telegram);


                if ($user) {

                    foreach (User::getChildrenByLine($user, 3) as $user) {
                        $list .= $i . '. <strong>' . $user->name . '</strong>';

                        $list .= "\n";
                        $i++;
                    }

                    $reply = $list . "\n\nChoose a further action";

                    $keyboard = [["1-st line", "2-st line", "3-st line"], [hex2bin('F09F8FA0') . " Main menu"]];
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                } else {
                    $reply = "<strong>Error! Unauthorized action. User: $chat_id</strong>\n";

                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Error! Unauthorized action. Log in or register again</strong> \n";
                    $keyboard = [["/start"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }
            } elseif ($text == hex2bin('F09F92B2') . " Reinvest") {
                $user = User::where('telegram_id', $chat_id)->first();
                $reply = "You can enter an amount in the text field at least <strong>10$</strong>. \n Or choose from the proposed options of Reinvest.";
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"], [hex2bin('F09F92B0') . " My deposits"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup, $disable_web_page_preview = true);


                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'disable_web_page_preview' => true, 'text' => $reply, 'reply_markup' => $reply_markup ]);

                $inlineLayout = [[
                    Keyboard::inlineButton(['text' => 'Все', 'callback_data' => "reinvest_all"]),
                ],
                    [
                        Keyboard::inlineButton(['text' => '50$', 'callback_data' => "reinvest_50"]),
                        Keyboard::inlineButton(['text' => '100$', 'callback_data' => "reinvest_100"]),
                        Keyboard::inlineButton(['text' => '150$', 'callback_data' => "reinvest_150"]),
                        Keyboard::inlineButton(['text' => '200$', 'callback_data' => "reinvrst_200"]),
                    ], [
                        Keyboard::inlineButton(['text' => '300$', 'callback_data' => "reinvest_300"]),
                        Keyboard::inlineButton(['text' => '400$', 'callback_data' => "reinvest_400"]),
                        Keyboard::inlineButton(['text' => '500$', 'callback_data' => "reinvest_500"]),
                        Keyboard::inlineButton(['text' => '600$', 'callback_data' => "reinvest_600"]),
                    ], [
                        Keyboard::inlineButton(['text' => '700$', 'callback_data' => "reinvest_700"]),
                        Keyboard::inlineButton(['text' => '800$', 'callback_data' => "reinvest_800"]),
                        Keyboard::inlineButton(['text' => '900$', 'callback_data' => "reinvest_900"]),
                        Keyboard::inlineButton(['text' => '1000$', 'callback_data' => "reinvest_1000"]),
                    ]
                ];

                $inline_keyboard = Telegram::replyKeyboardMarkup([
                    'inline_keyboard' => $inlineLayout,
                ]);

                if ($int_balance = $user->internal_balances()->where('currency', 'USD')->first())
                    $balance = $int_balance->amount;
                else
                    $balance = 0.0000;
                $reply = "Available balance <b>" . round($balance, 2) . "$</b>. \n Select how many you want to Reinvest.";
                $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $inline_keyboard ]);

                $this->setCommand($chat_id, "reinvest_money");

            } elseif ($text == hex2bin('F09F998B') . " Support") {
                $reply = "To connect to the operator, press the button <strong>\"Start a chat\"</strong>";
                    $inlineLayout = [[
                        Keyboard::inlineButton(['text' => 'Start a chat', 'url' => "https://t.me/FRSProject"]),
                    ]];
                    $inline_keyboard = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
                    $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);

            } elseif ($text == hex2bin('E29C8F') . " Shared chat") {
                $reply = "To go to Shared chat, click the button <strong>\"Start a chat\"</strong>";
                    $inlineLayout = [[
                        Keyboard::inlineButton(['text' => 'Start a chat', 'url' => "https://t.me/joinchat/Hc_M2BG5QqnKIAzs410nEA"]),
                    ]];
                    $inline_keyboard = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
                    $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);

            }  elseif ($text == hex2bin('F09F93B0') . " News") {
				$news = News::take(5)->get();
				
				$list = '';
				foreach ($news as $record) {

                        $list .= '['.$record->created_at->format('d/m/Y').'] <strong>' . $record->title . '</strong>';
                        $list .= "\n";
                        $list .= $record->text;
                        $list .= "\n";
                    }
                $reply = $list;
                    $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
					
					
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            }  elseif ($text == hex2bin('e29d93') . " FAQ") {
                $reply = "
				<strong>What is FRS Project?</strong>
FRS Project is functional back-up system. This is a trading system with the creation of reserve capital for each transaction once.
<strong>What are the ways of investing?</strong>
Bitcoin, Advcash, Ripple, Perfect Money, Ethereum.
<strong>Can I add some amount to an already created deposit and thus increase the nominal value of the investment?</strong>
Yes, it’s possible.
<strong>What is an affiliate program?</strong>
This is a program that provides for the accrual and payment of partner reward to users, whose invited partners have created an investment.
<strong>What are the conditions for assessment of the partner bonus?</strong>
Your partner must provide your username in section «My inviter»
<strong>How many referral lines and what percentage of profit?</strong>
There are 3 referral lines with profitability.
🥇For attracting of the partner in trust management - 3% from the deposit and 10% from the received profit.
🥈For attracting a partner in trust management, the 2-nd line is 2.5% from the deposit and 7%  from the received profit.
🥉For attracting a partner into trust management, the 3-rd line is 2% from the deposit and 5% from the received profit.
<strong>What determines the profitability of the deposit?</strong>
The profitability directly depends on the amount of investment.
  10$ - 100$ - 1.6% per day.
  100$ - 500$ - 1.8% per day.
  500$ - 2500$ - 2.2% per day.
2500$ и более - 2.5% per day.
<strong>How often does the withdrawal of interest or the principal of deposit??</strong>
The withdrawal takes place every Sunday as agreed.

				";
                    $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
					
					
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            }   elseif ($text == hex2bin('f09f938c') . " About us") {
                $reply = "
				About the project.
The project «FRS-Functional Reserve System» is a project with a functional reserve capital system. There are more than 20 successful trading schemes and trade in different areas. However, the target area is trading of crypto-currency also. At the moment, FRS has 3 directions in which it places its assets. These niches are actively developing on the international scene now. Thanks to these areas, it is possible to receive a constant income, and the companies steadily take their positions and can progressively develop on the market.

The project has begun its activities from the sale of oil, later there added such assets as gold and stocks. The direction of crypto currency is certainly new. Six years ago, nobody heard about such asset. The market of crypto-currency is very risky and does not suffer mistakes. In addition, we can say, that this market is primitive.  \"FRS\" is resistant to any changes on the market, because our statisticians and financial analysts regularly monitor the changes. We have seen many ups and downs of crypto-currency and such assets as oil or stocks. In the case of force majeure, we immediately adopt strategies for eliminating risk in a particular sector.
				";
                    $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
					
					
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            } elseif ($text == hex2bin('F09F8FA0') . " Main menu") {
                $user = $this->getUser($chat_id, $telegram);

                if ($user) {
                    $this->clearCommand($user);
                    $reply = "Choose a further action";
                    $keyboard = $mainMenu;
                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                    //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);
                } else {
                    $reply = "<strong>Error! Unauthorized action. User: $chat_id</strong>\n";
                    $this->sendMessage($telegram, env('TELEGRAM_ADMIN_ID'), $reply);

                    $reply = "<strong>Error! Unauthorized action. Log in or register again</strong> \n";
                    $keyboard = [["/start"]];

                    $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                    $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
                }


            } elseif ($text == hex2bin('F09F9396') . " Marketing") {

                $this->clearCommand($user);
                $reply = "
Маркетинг план доверительного управления FRS Project
   Депозит возвратный, минимальный срок 30 дней. 
   До 5000 $ прибыль делится 50 на 50 (между партнером и доверительным управлением )
   Суммы, которые выше 5000 $ ( Условия обговариваются лично ) 
   Минимальный депозит от 50$
   Минимальный вывод 10$
Начисления процентов обновляются на балансе каждый день в нашем телеграмм бот.
 Вывод каждые выходные.
 Реферальная система: Начисления каждый день, вывод каждые выходные
    🥇За привлечение партнера в доверительное управление - 3% от депозита и 10% от   полученной прибыли
    🥈За привлечение партнера в доверительное управление 2 линия - 2.5% от депозита и 7% от полученной прибыли
    🥉За привлечение партнера в доверительное управление 3 линия - 2% от депозита и 5% от полученной прибыли
    Доступные способы ввода инвестиций.
    Bitcoin, ADVcash, Perfect Money, Ethereum и другие валюты.";
                $keyboard = $mainMenu;
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

            } elseif ($text == "Set a inviter") {
                $reply = "Referral system: The accruals are every day, every Saturday, Sunday.
🥇For attracting of the partner in trust management - 3% from the deposit and 10% from the received profit.
🥈For attracting a partner in trust management, the 2-nd line is 2.5% from the deposit and 7%  from the received profit. 
🥉For attracting a partner into trust management, the 3-rd line is 2% from the deposit and 5% from the received profit. 
🏦Available ways of entering investments 💵  
💎Bitcoin, Advcash, Ripple, Perfect Money, Ethereum💰

		Send the nickname of the person who invited you (ex.: @FRSProject)";

                $keyboard = [["Install inviter later"]];
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                //$telegram->sendMessage([ 'chat_id' => $chat_id, 'parse_mode'=> 'HTML', 'text' => $reply, 'reply_markup' => $reply_markup ]);

                $this->setCommand($chat_id, "set_refer");
            } elseif ($text == "Perfect Money") {
				$this->setCommand($chat_id, "perfect-money");
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"], [hex2bin('F09F92B0') . " My deposits"]];
                $reply = "You can enter an amount in the text field at least 10$. \n Or choose from the proposed.";
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
                $this->sendMessage($telegram, $chat_id, $reply, $reply_markup);

                $inlineLayout = [
                    [
                        Keyboard::inlineButton(['text' => '50$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/50"]),
                        Keyboard::inlineButton(['text' => '100$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/100"]),
                        Keyboard::inlineButton(['text' => '150$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/150"]),
                        Keyboard::inlineButton(['text' => '200$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/200"]),
                    ], [
                        Keyboard::inlineButton(['text' => '300$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/300"]),
                        Keyboard::inlineButton(['text' => '400$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/400"]),
                        Keyboard::inlineButton(['text' => '500$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/500"]),
                        Keyboard::inlineButton(['text' => '600$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/600"]),
                    ], [
                        Keyboard::inlineButton(['text' => '700$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/700"]),
                        Keyboard::inlineButton(['text' => '800$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/800"]),
                        Keyboard::inlineButton(['text' => '900$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/900"]),
                        Keyboard::inlineButton(['text' => '1000$', 'url' => "https://frs-project.com/replenish/perfect-money/$chat_id/1000"]),
                    ]
                ];

                $inline_keyboard = Telegram::replyKeyboardMarkup([
                    'inline_keyboard' => $inlineLayout,
                ]);

                $reply = "Select amoutn of refill";
                $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);
                //$this->setCommand($chat_id, "invest_money");
            } elseif ($text == "Advanced Cash") {
				$this->setCommand($chat_id, 'advcash');
                $keyboard = [[hex2bin('F09F8FA0') . " Main menu"]];
                $reply = "Dear Partners, temporary replenishment through \"Advanced Cash \" is accepted manually, for recharge, go to the chat with our administration.";
                $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
				$this->sendMessage($telegram, $chat_id, $reply, $reply_markup);
				 $inlineLayout = [[
                        Keyboard::inlineButton(['text' => 'Start a chat', 'url' => "https://t.me/FRSProject"]),
                    ]];
                    $inline_keyboard = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
				$reply = 'Yours faithfully, FRS Project!';
                $this->sendMessage($telegram, $chat_id, $reply, $inline_keyboard);
                //$this->setCommand($chat_id, "invest_money");
            } else {
                $reply = "On request \"<b>" . $text . "</b>\" nothing found.";
                $this->sendMessage($telegram, $chat_id, $reply);

            }
        }
    }
}
