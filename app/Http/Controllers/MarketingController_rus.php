<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balance;

class MarketingController extends Controller
{
    public function start(Request $request)
    {
		
		//$input_percent = $request->amount;
		$input_percent = 20;

        $this->validate($request, ['amount' => 'required']);
		$percent = $input_percent  / 100;
		
		foreach (Balance::all() as $balance) {
			
			$int_balance = $balance->internal_balances()->where('currency', $balance->currency)->first();
			
			$int_balance->increment('amount', round($balance->amount * $percent, 3));
			
			$history = new \App\BalanceHistory([
				'amount' => round($balance->amount * $percent, 3),
				'source' => 'Очередное начисление',
				'description' => 'Начисление на депозит №<strong>'.$balance->id.'</strong> в размере <strong>'.$input_percent.'%</strong> от тела (<strong>'.$balance->amount.' '.$balance->currency.'</strong>)'
			]);

			$int_balance->history()->save($history);
			
			//Проводка по маркетингу
            $ref_percent = 0;

            for ($i=1; $i <= 3; $i++) {

                if ($i == 1) {
                    $parent = $balance->user->parent;
					$ref_percent = 10/100;
                }
                if ($i == 2 && $parent) {
                    $parent = $balance->user->parent->parent;
					$ref_percent = 5/100;
                }
                if ($i == 3 && $parent) {
                    $parent = $balance->user->parent->parent->parent;
					$ref_percent = 5/100;
                }


                if ($parent) {
                        $award = round($balance->amount * $percent * $ref_percent, 3);
                        $parent_balance = $parent->balance->internal_balances()->where('currency', $balance->currency)->first();
						
						if($parent_balance) {
							$parent_balance->increment('amount', $award);

							$history = new \App\BalanceHistory([
								'amount' => $award,
								'source' => 'Реферальная система',
								'description' => 'Начисление реферальных с партнера <strong>'.$i.'</strong> линии </strong>'.$balance->user->name.'</strong>'
							]);

							$parent_balance->history()->save($history);	
						}
                }
            }
		}

        return 'Начисления прошли в полном объёме!';

    }
}
