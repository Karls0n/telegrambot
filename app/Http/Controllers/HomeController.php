<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    private function getPercent($balance)
    {
        $percent = 0;

        if($balance >= 10) {
            $percent = 1.6;
        }

        if($balance >= 100){
            $percent = 1.8;
        }

        if($balance >= 500){
            $percent = 2.2;
        }

        if($balance >= 2500){
            $percent = 2.5;
        }

        
        return $percent;
    }

    public function chargePercent()
    {
		foreach (\App\Balance::all() as $balance) {
			
			$int_balance = $balance->internal_balances()->where('currency', $balance->currency)->first();
			echo $balance->internal_balances;
			if($int_balance) {
				$int_balance->increment('amount', round($balance->amount * ($this->getPercent($balance->amount) / 100), 3));

				echo '<br>'.$int_balance->amount;

                $history = new \App\BalanceHistory([
                    'user_id' => $balance->user->id,
                    'amount' => round($balance->amount * ($this->getPercent($balance->amount) / 100), 3),
                    'currency' => $balance->currency,
                    'source' => 'Очередное начисление',
                    'description' => 'Начисление на депозит №'.$balance->id.' в размере '.$this->getPercent($balance->amount).'% от тела ('.$balance->amount.' '.$balance->currency.')'
                ]);

                $int_balance->history()->save($history);

                //Проводка по маркетингу
                for ($i=1; $i <= 3; $i++) {

                    if ($i == 1) {
                        $parent = $balance->user->parent;
                        $ref_percent = 10/100;
                    }
                    if ($i == 2 && $parent) {
                        $parent = $balance->user->parent->parent;
                        $ref_percent = 7/100;
                    }
                    if ($i == 3 && $parent) {
                        $parent = $balance->user->parent->parent->parent;
                        $ref_percent = 5/100;
                    }


                    if ($parent) {
                        if($parent->balances) {

                            $award = round($balance->amount * ($this->getPercent($balance->amount) / 100) * $ref_percent, 3);
                            $parent_balance = $parent->internal_balances()->where('currency', $balance->currency)->first();

                            if($parent_balance) {
                                $parent_balance->increment('amount', $award);

                                $history = new \App\BalanceHistory([
                                    'user_id' => $parent->id,
                                    'amount' => $award,
                                    'currency' => $balance->currency,
                                    'source' => 'Партнерская программа ур.'.$i,
                                    'description' => 'Начисления от партнера '.$i.' линии '.$balance->user->name.''
							]);

                                $parent_balance->history()->save($history);
                            }
                        }
                    }
                }
			}
		}

        Session::flash('flash_message', 'Начисления выполнены в полном объеме!');

        return redirect()->back();

    }
	
	public function chargePercentCustom(Request $request)
    {
		foreach (\App\Balance::all() as $balance) {
			
			$int_balance = $balance->internal_balances()->where('currency', $balance->currency)->first();
			if($int_balance) {
				$int_balance->increment('amount', round($balance->amount * ($request->sum / 100), 3));

				echo '<br>'.$int_balance->amount;

                $history = new \App\BalanceHistory([
                    'user_id' => $balance->user->id,
                    'amount' => round($balance->amount * ($request->sum / 100), 3),
                    'currency' => $balance->currency,
                    'source' => 'Очередное начисление',
                    'description' => 'Начисление на депозит №'.$balance->id.' в размере '.$request->sum.'% от тела ('.$balance->amount.' '.$balance->currency.')'
                ]);

                $int_balance->history()->save($history);

                //Проводка по маркетингу
                for ($i=1; $i <= 3; $i++) {

                    if ($i == 1) {
                        $parent = $balance->user->parent;
                        $ref_percent = 10/100;
                    }
                    if ($i == 2 && $parent) {
                        $parent = $balance->user->parent->parent;
                        $ref_percent = 7/100;
                    }
                    if ($i == 3 && $parent) {
                        $parent = $balance->user->parent->parent->parent;
                        $ref_percent = 5/100;
                    }


                    if ($parent) {
                        if($parent->balances) {

                            $award = round($balance->amount * ($request->sum / 100) * $ref_percent, 3);
                            $parent_balance = $parent->internal_balances()->where('currency', $balance->currency)->first();

                            if($parent_balance) {
                                $parent_balance->increment('amount', $award);

                                $history = new \App\BalanceHistory([
                                    'user_id' => $parent->id,
                                    'amount' => $award,
                                    'currency' => $balance->currency,
                                    'source' => 'Партнерская программа ур.'.$i,
                                    'description' => 'Начисления от партнера '.$i.' линии '.$balance->user->name.''
                                ]);

                                $parent_balance->history()->save($history);
                            }
                        }
                    }
                }
			}
		}

        Session::flash('flash_message', 'Начисления выполнены в полном объеме!');

        return redirect()->back();

    }

    public function index()
    {
        $users = \App\User::all();

        return view('home', compact('users'));
    }

    public function chargeMoney(Request $request)
    {
        $user = \App\User::where('telegram_id', $request->user)->first();

        if(!$int_balance = \App\InternalBalance::where('user_id', $user->id)->where('currency', $request->currency)->first()) {
            $int_balance = new \App\InternalBalance([
                'user_id' => $user->id,
                'amount' => 0,
                'currency' => $request->currency
            ]);

            $int_balance->save();
        } else {
            $int_balance = \App\InternalBalance::where('user_id', $user->id)->where('currency', $request->currency)->first();
        }


        $balance = new \App\Balance([
            'user_id' => $user->id,
            'amount' => $request->sum,
            'currency' => $request->currency
        ]);

        $balance->save();


        $int_balance->balances()->attach($balance->id);


        $history = new \App\BalanceHistory([
            'user_id' => $user->id,
            'amount' => $request->sum,
            'currency' => $request->currency,
            'source' => 'Administrator',
            'description' => 'Размещение нового депозита'
        ]);

        $balance->history()->save($history);
		
		// проводка по маркетингу
                for ($i=1; $i <= 3; $i++) {


                    if ($i == 1) {
                        $parent = $balance->user->parent;
                        $ref_percent = 3/100;
                    }
                    if ($i == 2 && $parent) {
                        $parent = $balance->user->parent->parent;
                        $ref_percent = 2.5/100;
                    }
                    if ($i == 3 && $parent) {
                        $parent = $balance->user->parent->parent->parent;
                        $ref_percent = 2/100;
                    }
			

                    if ($parent) {
                        if($parent->balances) {

                            $award = round($balance->amount * $ref_percent, 3);
                            $parent_balance = $parent->internal_balances()->where('currency', $balance->currency)->first();
                            if($parent_balance) {
                                $parent_balance->increment('amount', $award);

                                $history = new \App\BalanceHistory([
                                    'user_id' => $parent->id,
                                    'amount' => $award,
                                    'currency' => $balance->currency,
                                    'source' => 'Партнерская программа ур.'.$i,
                                    'description' => 'Начисления от партнера '.$i.' линии '.$balance->user->name.''
                                ]);

                                $parent_balance->history()->save($history);
                            }
                        }
                    }
                }
		Session::flash('flash_message', 'Начисления выполнены в полном объеме!');

        return redirect()->back();

    }
	
	public function messageSender(){
		$hook = new WebhookController;
		$telegram = new Api(env('TELEGRAM_BOT_TOKEN')); //Устанавливаем токен, полученный у BotFather
		$inlineLayout = [[
                        Keyboard::inlineButton(['text' => '🇷🇺 Русский чат ', 'url' => "https://t.me/joinchat/Hc_M2EytQXtWEHkkY2MFaA"]),
                        Keyboard::inlineButton(['text' => '🇺🇿 Uzbekskiy chat', 'url' => "https://t.me/frsprojectUZ"]),
                    ]];
        $reply_markup = Telegram::replyKeyboardMarkup([
                        'inline_keyboard' => $inlineLayout,
                    ]);
		
		$reply = "<b>Доброго времени суток.</b>.\nПереходите в наши чаты для знакомства с FRS.\n\nС Ув. FRS Project.";
		//$chat_id = 185030948;
		
		$i = 1;
		foreach (\App\User::get()->pluck('telegram_id') as $chat_id) {
		
			$hook->sendMessage($telegram, $chat_id, $reply, $reply_markup);
		
			print $i.'. Сообщение пользователю <strong>'.$chat_id.'</strong> успешно доставлено.<br>';
			$i++;
		}
		
		//return $hook->sendMessage($telegram, $chat_id, $reply);
//    public function sendMessage($telegram, $chat_id, $reply, $reply_markup = null, $disable_web_page_preview = null)
		
		
	}
}
