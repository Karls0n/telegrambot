<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
     protected $table = 'balances';

    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function history()
    {
        return $this->morphMany('App\BalanceHistory', 'historyable');
    }

    public function internal_balances()
    {
        return $this->belongsToMany('App\InternalBalance')->withTimestamps();
    }

}
