<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawalApplication extends Model
{
     protected $table = 'withdrawal_applications';

    protected $dates = ['is_completed'];

    private $display_name = [
        'new' => 'В обработке',
        'completed' => 'Выполнена',
        'declined' => 'Отменена',
    ];

    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\User');
    }
	
    public function internal_balance() {
        return $this->belongsTo('App\InternalBalance');
    }

    public function getStatus() {
        //return $this->display_name[$this->status];
        return $this->status;
    }
}
