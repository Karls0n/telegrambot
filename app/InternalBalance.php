<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternalBalance extends Model
{
    protected $table = 'internal_balances';

    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function history()
    {
        return $this->morphMany('App\BalanceHistory', 'historyable');
    }

    public function balances()
    {
        return $this->belongsToMany('App\Balance')->withTimestamps();
    }

    public function withdrawal_application()
    {
        return $this->hasOne('App\WithdrawalApplication');
    }

}
