<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/test', function (){
    $user = App\User::find(775);
	$maxDepo = 0;
	foreach($user->users as $child) {
		if($child->balances) {
			$maxPerUser = $child->balances->sortByDesc('amount')->pluck('amount')->take(1);		
			if($maxPerUser->first() > $maxDepo) {
				$maxDepo = $maxPerUser->first();
			}
		}
	}
	
	
	
	
	return $maxDepo;
	
	
});

Route::get('/about-us', function (){
    return view('about');
});
Route::get('/why', function (){
    return view('why');
});
Route::get('/contacts', function (){
    return view('contacts');
});

Route::get('/register', function (){
    return redirect()->to('/');
});

Route::get('/logout', function (){
    Auth::logout();
    return redirect()->to('/');
});

Route::get('/', function () {
    return view('landing');
});

Route::post('/' . env('TELEGRAM_BOT_TOKEN'), 'WebhookController@index');

Route::get('/' . env('TELEGRAM_BOT_TOKEN'), function () {
        Log::info('Привет из телеграма! get');
         return response('Hello World', 200)
                  ->header('Content-Type', 'text/plain');
});

Route::group(['prefix' => 'replenish', 'as' => 'replenish.'], function() {
    Route::get('/', function(){
	return response('Page not found. Error 404.', 404)
                  ->header('Content-Type', 'text/plain');
    });

   Route::group(['prefix' => 'perfect-money', 'as' => 'perfect-money.'], function (){
	Route::get('/{user_id}/{money}', 'PerfectMoneyController@index');
   	Route::post('/status', 'PerfectMoneyController@status');
	Route::post('/success', 'PerfectMoneyController@success');
	Route::post('/failed', 'PerfectMoneyController@failed');
   });
   
   Route::group(['prefix' => 'advcash', 'as' => 'perfect-money.'], function (){
	Route::get('/{user_id}/{money}', 'AdvCashController@index');
   	Route::post('/status/{user_id}', 'AdvCashController@status');
	Route::post('/success', 'AdvCashController@success');
	Route::post('/failed', 'AdvCashController@failed');
   });
});

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/charge-money', 'HomeController@chargeMoney')->name('home.chargeMoney');
Route::post('/home/charge-percent', 'HomeController@chargePercent')->name('home.chargePercent');
Route::post('/home/charge-percent-custom', 'HomeController@chargePercentCustom')->name('home.chargePercentCustom');
Route::get('/message-sender', 'HomeController@messageSender')->name('messageSender');
