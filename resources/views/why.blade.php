@extends('layouts.landing')


@section('content')
<div id="content">
    <!--==============================row10=================================-->
    <div class="row_10" style="padding: 18px 0 64px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9">
                    <h2>Почему мы?</h2>
                    <h3>Пять причин, по которым стоит начать работать с «FRS Project»</h3>
                    <p class="m_bot3">
					<strong>1. Надежность</strong><br>
В работе на криптовалютном рынке надежность  это ключевой фактор успеха. Там где вопросы касаются денег, это главное. «FRS Project» – серьезный партнер, призванный обеспечить надежность и сохранность активов своих клиентов. Надежные платформы, связь и внутренний контроль – наш стандарт.					</p>
					<p class="m_bot3">
					<strong>2. Инвестиции только в проверенные активы</strong><br>
Не довольствуйтесь малым, открывайте для себя весь мир. Ваши возможности безграничны. Инвестируйте в FRS Project. Присмотритесь к технологичным отраслям, которые сделают прорыв в будущем. Вы консерватор? Накапливайте свои криптоактивы.
					</p>
					<p class="m_bot3">
					<strong>3. Уникальность</strong><br>
FRS производит торговые идеи, которые работают. Наша команда строит простые и сложные инвестиционные стратегии, которые ограничивают риск и позволяют зарабатывать даже при падении. Мы используем все инвестиционные возможности для своих клиентов и самые передовые инструменты.					</p>
					<p class="m_bot3">
					<strong>4. Четкие алгоритмы работы и бизнес-процессы.</strong><br>
Благодаря грамотно построенной  предварительной работе нашим клиентам не приходится надолго отрываться от своих  текущих дел. На ввод, получения отчетности и вывод средств, затраты времени минимальны. Мы ценим время наших клиентов.
					</p>
					<p class="m_bot3">
					<strong>5. Высокая компетентность.</strong><br>
Все наши сотрудники являются профессионалами в своей 
области. У нас индивидуальный подход к каждому клиенту. Наши клиенты получают 
четкие ответы на свои вопросы и быстрое решение своих задач.

                    <div class="row core">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <h3>Бухгалтерский учет</h3>
                            <p>
							Благодаря пристальному ведению бухгалтерского учета, мы четко знаем, в какие активы направлен каждый цент доверительного управления. И четко прогнозируем временной интервал работы, того или иного криптоактива.
							</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <h3>Финансовая устойчивость</h3>
                            <p>Наши устойчивые позиции капитала, консервативный подход к бухгалтерскому учету и автоматизированный контроль рисков - все это помогает защитить FRS Project  и ее инвесторов от торговых убытков.</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <h3>Управление рисками</h3>
                            <p>
							FRS Project ведет ежедневный целенаправленный поиск и организацию работы по снижению степени риска и увеличению дохода. 
Разумное управление рисками это один из ключевых приоритетов в управлении средствами клиентов. Взвешенный и разумный подход к управлению рисков гарант стабильности и долголетия.
							</p>
                        </div>
                    </div>
                    <hr class="line1">
                    <h2 class="bottom1 color1">Видео-отзывы наших партнеров:</h2>
                    <div class="row">
                        <ul class="list5">
                            <li class="col-lg-4 col-md-4 col-sm-4 col-xs-4 collist5">
                                <figure>
								<iframe width="270" height="146" src="https://www.youtube.com/embed/5NmJMK9hlnU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								</figure>  
                            </li>
                            <li class="col-lg-4 col-md-4 col-sm-4 col-xs-4 collist5">
                                <figure><iframe width="270" height="146" src="https://www.youtube.com/embed/L6dT_0xQscY?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></figure>
                            </li>
                            <li class="col-lg-4 col-md-4 col-sm-4 col-xs-4 collist5">
                                <figure>Место для Вашего обзора</figure>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 approach">
                    <h2 class="bottom1">FRS-Functional Reserve System</h2>
                    <figure><img src="landing/assets/images/frs-logo.png" alt=""></figure>
                    <!--<h3>Знакомство с FRS</h3>
                    <p class="m_bot1">Что такое FRS Project?</p>
                    <p>FRS Project- Функциональная резервная система. Это система трейдинга с созданием резервного капитала на каждую сделку единоразово.</p>
                    <ul class="list4">
                        <li>
                           <img src="landing/assets/images/arrow1.png" alt=""><a href="#">Vestibulum ante ipsum primisn faucibus orciluctus et ltrices posuere cubilia</a>
                        </li>
                        <li>
                           <img src="landing/assets/images/arrow1.png" alt=""><a href="#">Suspendisse sollicitudin velit sed leot pharetra augue nec augue</a>
                        </li>
                        <li>
                           <img src="landing/assets/images/arrow1.png" alt=""><a href="#">Nam elit agna endrerit sit amet incidunt ac viverra sed nulla</a>
                        </li>
                        <li>
                           <img src="landing/assets/images/arrow1.png" alt=""><a href="#">Donec porta diam eu massa uisque diam lorem nterdum vitae dapibus ac</a>
                        </li>
                        <li>
                           <img src="landing/assets/images/arrow1.png" alt=""><a href="#">Scelerisque vitae pede onec eget tellus non erat acinia fermentum</a>
                        </li>
                        <li>
                           <img src="landing/assets/images/arrow1.png" alt=""><a href="#">Donec in velit vel ipsum auctor pulvinar estibulum iaculis lacinia est</a>
                        </li>
                        <li>
                           <img src="landing/assets/images/arrow1.png" alt=""><a href="#">Proin dictum elementum velit usce euismod consequat ante</a>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection