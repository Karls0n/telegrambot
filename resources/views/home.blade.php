@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Administration [<a href="/logout">Выйти</a>]</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <blockquote>
                            Начислить указаную сумму определенному пользователю.
                        </blockquote>

                    <form action="/home/charge-money" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label for="user">Пользователь</label>

                                <select class="form-control" name="user">
                                    @foreach($users as $user)
                                        <option value="{{ $user->telegram_id }}">{{ $user->name }}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="form-group col-lg-6">
                                <label for="sum">Сумма</label>
                                <input class="form-control" name="sum" type="text">
                            </div>

                            <div class="form-group col-lg-6">
                                <label for="currency">Валюта</label>
                                <select class="form-control" name="currency">
                                    <option value="usd">USD</option>
                                </select>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="form-group col-lg-2 col-lg-offset-9">
                                <button class="btn btn-primary ">Начислить</button>
                            </div>
                        </div>
                    </form>

                        <hr>

                        <blockquote>
                            Начислить процент всем пользователям, в зависимости суммы на балансе.
                        </blockquote>

                    <form action="/home/charge-percent" method="POST">
                        {{ csrf_field() }}

                        <div class="row ">
                            <div class="form-group col-lg-2 col-lg-offset-9">
                                <button class="btn btn-primary ">Начислить</button>
                            </div>
                        </div>
                    </form>
                        <hr>

                        <blockquote>
                            Начислить прибыль в % на все депозиты.
                        </blockquote>

                    <form action="/home/charge-percent-custom" method="POST">
                        {{ csrf_field() }}

                        <div class="row ">
                            <div class="form-group col-lg-6">
                                <input type="number" name="sum" min="0.01" step="0.01" value="0.01" style="width: 100%; margin-left: 10px;">
                            </div>
                        </div>
                        <div class="row ">
                            <div class="form-group col-lg-2 col-lg-offset-9">
                                <button class="btn btn-primary ">Начислить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection