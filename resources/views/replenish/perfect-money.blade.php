<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
			
    <img src="https://cdn.dribbble.com/users/15774/screenshots/1759511/spinner.gif" alt="">
<h2 class="" style="text-transform: uppercase; margin-top: -25px;">Автоматическая переадресация...</h2>
			
		<form action="https://perfectmoney.is/api/step1.asp" method="POST">
    			<input type="hidden" name="PAYEE_ACCOUNT" value="{{env('PM_ACCOUNT')}}">
    			<input type="hidden" name="PAYEE_NAME" value="{{env('APP_NAME')}}">
    			<input type="hidden" name="PAYMENT_ID" value="{{rand(99999,999999)}}" readonly>
    			<input type="hidden" name="PAYMENT_AMOUNT" value="{{$money}}">
    			<input type="hidden" name="PAYMENT_UNITS" value="USD">
    			<input type="hidden" name="STATUS_URL" value="{{url('/')}}/replenish/perfect-money/status">
    			<input type="hidden" name="PAYMENT_URL" value="https://telegram.me/FRSProjectBot">
    			<input type="hidden" name="PAYMENT_URL_METHOD" value="LINK">
    			<input type="hidden" name="NOPAYMENT_URL" value="https://telegram.me/FRSProjectBot">
    			<input type="hidden" name="NOPAYMENT_URL_METHOD" value="LINK">
    			<input type="hidden" name="SUGGESTED_MEMO" value="">
    			<input type="hidden" name="BAGGAGE_FIELDS" value="USERID">
    			<input type="hidden" name="USERID" value="{{$id}}">
    			<input type="submit" style="display:none;" name="PAYMENT_METHOD" value="Оплатить">
		</form>
            </div>
        </div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script>
		$( document ).ready(function() {
			$('form').submit();
		});
		</script>
    </body>
</html>
