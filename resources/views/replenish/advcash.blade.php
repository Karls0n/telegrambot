<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
			
    <img src="https://cdn.dribbble.com/users/15774/screenshots/1759511/spinner.gif" alt="">
<h2 class="" style="text-transform: uppercase; margin-top: -25px;">Автоматическая переадресация...</h2>
			
			<form method="post" action="https://wallet.advcash.com/sci/">
				 <input type="hidden" name="ac_account_email" value="{{$ac_email}}" />
				 <input type="hidden" name="ac_sci_name" value="{{$sci_name}}" />
				 <input type="hidden" name="ac_amount" value="{{$money}}" />
				 <input type="hidden" name="ac_currency" value="{{$ac_currency}}" />
				 <input type="hidden" name="ac_order_id" value="{{$order_id}}" />
				 <input type="hidden" name="ac_sign" value="{{$ac_sign}}" />
				 <!-- Optional Fields -->
				 <input type="hidden" name="ac_success_url" value="https://frs-project.com/replenish/advcash/redirect" />
				 <input type="hidden" name="ac_success_url_method" value="GET" />
				 <input type="hidden" name="ac_fail_url" value="https://frs-project.com/replenish/advcash/redirect" />
				 <input type="hidden" name="ac_fail_url_method" value="GET" />
				 <input type="hidden" name="ac_status_url" value="https://frs-project.com/replenish/advcash/status/{{$id}}" />
				 <input type="hidden" name="ac_status_url_method" value="POST" />
				 <input type="submit" style="display:none;" />
			</form>
            </div>
        </div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script>
		$( document ).ready(function() {
			$('form').submit();
		});
		</script>
    </body>
</html>
