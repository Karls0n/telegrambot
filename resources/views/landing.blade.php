@extends('layouts.landing')
@section('head-css')
    <link rel="stylesheet" href="landing/assets/css/camera.css">
@endsection
@section('head-scripts')
    <script src='landing/assets/js/camera.js'></script>
@endsection

@section('content')
 <!--==============================slider=================================-->
    <div class="container">
        <div class="row_1">
            <div class="slider">
                <div class="camera_wrap">
                  <div data-src="landing/assets/images/empty.png">
                    <div class="camera_caption">
                        <p>FRS Project</br>Functional Reserve System</p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!--==============================row2=================================-->
    <div class="row_2">
        <div class="container">
            <div class="row">
                <ul class="list1">
                    <li class="col-lg-3 col-md-3 col-sm-3">
                        <div class="topicons">
                            <figure><img src="landing/assets/images/page1_icon1.png" alt=""></figure>
                            <div class="iconright">
                                <div class="headicon1">
                                    <p>1. Изучение</p>
                                    <em></em>
                                </div>
                                <div class="headicon2">
                                <em></em>
                                </div>
                            </div>
                        </div>
                        <div class="text1">
                            <p>Детально ознакомиться с информацией на сайте</p>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-3">
                        <div class="topicons">
                            <figure><img src="landing/assets/images/page1_icon2.png" alt=""></figure>
                            <div class="iconright">
                                <div class="headicon1">
                                    <p>2. Установка</p>
                                    <em></em>
                                </div>
                                <div class="headicon2">
                                <em></em>
                                </div>
                            </div>
                        </div>
                        <div class="text1">
                            <p>Скачать Telegram и установить его на компьютер - это основной инструмент!</p>
                            <a href="https://telegram.org/" target="_blank" class="btn-link btn-link1">Скачать</a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-3">
                        <div class="topicons">
                            <figure><img src="landing/assets/images/page1_icon3.png" alt=""></figure>
                            <div class="iconright">
                                <div class="headicon1">
                                    <p>3. Создать кабинет</p>
                                    <em></em>
                                </div>
                                <div class="headicon2">
                                <em></em>
                                </div>
                            </div>
                        </div>
                        <div class="text1">
                            <p>Вступить в телеграмм бот. Через бота совершаются все финансовые операции</p>
                            <a href="https://telegram.me/FRSProjectBot?start=523369150" target="blank" class="btn-link btn-link1">Вступить</a>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-3">
                        <div class="topicons">
                            <figure><img src="landing/assets/images/page1_icon4.png" alt=""></figure>
                            <div class="iconright">
                                <div class="headicon1">
                                    <p>4. Общение</p>
                                    <em></em>
                                </div>
                                <div class="headicon2">
                                <em></em>
                                </div>
                            </div>
                        </div>
                        <div class="text1">
                            <p>Вступить в телеграмм чат для общения с партнерами и участниками FRS</p>
                            <a href="https://t.me/joinchat/HzH6vkytQXuAB86V9_0juw" target="blank" class="btn-link btn-link1">Вступить</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--==============================row3=================================-->
    <div class="row_3" style="display:none;">
        <div class="container">
            <p class="title1">FRS - ваша прибыль наши риски</p>
        </div>
        <div class="bgrow3"></div>
        <div class="bgrow3img"></div>
    </div>
</header>
<!--==============================content=================================-->
<div id="content">
    <!--==============================row4=================================-->
    <div class="row_4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h2>Миссия доверительного управления</h2>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 row4col1">
                            <p style="font-size: 15px; line-height: 18px;">Наша миссия дать лучший сервис для инвестиций в криптовалюту. FRS Project формулирует по-настоящему уникальную услугу. Мы предоставляем возможность развиваться и расти вместе с нами, быть современным инвестором, иметь свободу выбора и свободно принимать инвестиционные решения. Наша главная задача – качественно и непрерывно сопровождать вас на каждом этапе этого инвестиционного процесса.
							</p>
                        </div> 
                    </div> 
                    <h2 style="font-size:22px; padding:0;">Криптовалюта это...</h2>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 row4col1">
                            <p style="font-size: 15px; line-height: 18px;">Криптовалюта — это децентрализованная валюта, которая эмитируется и обращается в интернет-пространстве без контроля над транзакциями и платежами со стороны государств и банковских систем. Она не подвержена кризисным тенденциям, инфляции и другим глобальным процессам в макроэкономике и политике.
Курс криптовалюты значительно возрастает на протяжении коротких периодов времени. Подобная волатильность рынка позволяет легко и быстро получать действительно высокую прибыль на спекулятивных операциях.</p>
                        </div> 
                    </div>            
                </div>
               <div class="col-lg-3 col-md-3 col-sm-3 row4col2">
                    <h2>Обзор:</h2>
                      <ul class="list-quote2">
                          <li>
                              <p>
<iframe width="270" height="182" src="https://www.youtube.com/embed/L6dT_0xQscY?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							  </p>
                          </li>
                      </ul> 
                </div>
				<div class="col-lg-3 col-md-3 col-sm-3  row4col2">
                    <h2 style="font-size:20px;">Рекомендуемые биржи:</h2>
                        <ul class="list2" style="margin-bottom:5px;">
                            <li>
                               <img src="landing/assets/images/arrow1.png" alt=""><strong>
							   <a href="https://www.binance.com/" target="_blank">
							   Китайская биржа BINANCE
							   </a>
                            </li>
                            <li>
                               <img src="landing/assets/images/arrow1.png" alt=""><strong>
							   <a href="https://exmo.com/" target="_blank">
							   Биржа EXMO
							   </a>
                            </li>
						</ul>
						<h2 style="padding-top:0; font-size:20px;">Рекомендуемые кошельки:</h2>
                        <ul class="list2">						
                            <li>
                               <img src="landing/assets/images/arrow1.png" alt=""><strong>
							   <a href="https://advcash.com/" target="_blank">
							   AdvCash (АдваКеш)
							   </a>
                            </li>						
                            <li>
                               <img src="landing/assets/images/arrow1.png" alt=""><strong>
							   <a href="https://perfectmoney.is" target="_blank">
							   Perfect Money
							   </a>
                            </li>						
                            <li>
                               <img src="landing/assets/images/arrow1.png" alt=""><strong>
							   <a href="https://www.blockchain.com/" target="_blank">
							   Blockchain
							   </a>
                            </li>						
                            <li>
                               <img src="landing/assets/images/arrow1.png" alt=""><strong>
							   <a href="https://payeer.com/ru/" target="_blank">
							   PAYEER
							   </a>
                            </li>
                           
                        </ul> 
                </div>
            </div>
        </div>
    </div>
    <!--==============================row5=================================-->
    <div class="row_5">
        <div class="container">
            <div class="row">
                  <ul class="list-row5">
                    <li class="col-lg-3 col-md-3 col-sm-3 colrow5 maxheight">
                     <div class="badge_ badge">01.<em></em></div>
                        <div class="overflow">
                            <h3><a href="#">Безопасно</a></h3>
                            <p>Ваши данные защищены с помощью SSL-сертификата Comodo и дополнительного внутреннего шифрования.</p>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-3 colrow5 maxheight">
                      <div class="badge_ badge">02.<em></em></div>
                        <div class="overflow">
                            <h3><a href="#">Стабильно</a></h3>
                            <p>Мы еженедельно выплачиваем прибыль нашим инвесторам и открыто размещаем финансовую статистику.</p>
                        </div>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-3 colrow5 maxheight">
                      <div class="badge_ badge">03.<em></em></div>
                        <div class="overflow">
                            <h3><a href="#">Функционально</a></h3>
                            <p>Большинство операций проводится автоматически в боте Telegram.</p>
                        </div>
                    </li>
                  </ul>
                  <div class="col-lg-3 col-md-3 col-sm-3 colrow5 maxheight">
                    <p class="title5">FRS 2018</p>
                        <a href="https://telegram.me/FRSProjectBot?start=523369150" class="btn btn-primary btn1">
                            <div class="btnicon1">
                                <p>Присоединиться</p>
                                <em></em>
                            </div>
                            <div class="btnicon2">
                            <em></em>
                            </div>
                        </a>
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection