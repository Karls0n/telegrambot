<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'FRS Telegram Trade Bot') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	
	
    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/default.min.css"/>
</head>
<body>
    <div id="app">

        @yield('content')
    </div>

    <!-- Scripts -->
	<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>

    <script src="{{ asset('js/app.js') }}"></script>
	
	
@if(\Illuminate\Support\Facades\Session::has('flash_message'))
<script>
    $(document).ready(function () {
        alertify.alert('', '{{\Illuminate\Support\Facades\Session::get('flash_message')}}', function(){ alertify.success('{{\Illuminate\Support\Facades\Session::get('flash_message')}}'); });
    });
</script>
@endif
</body>
</html>
