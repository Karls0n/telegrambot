<!DOCTYPE html>
<html lang="ru">
<head>
    <title>FRS 2018</title>
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="landing/assets/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="landing/assets/images/favicon.ico" type="image/x-icon" />
    <meta name = "format-detection" content = "telephone=no" />
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="landing/assets/css/bootstrap.css" >
    <link rel="stylesheet" href="landing/assets/css/style.css">
	@yield('head-css')
    
	<script src="landing/assets/js/jquery.js"></script>
    <script src="landing/assets/js/jquery-migrate-1.2.1.js"></script>
    <script src="landing/assets/js/superfish.js"></script>
    <script src="landing/assets/js/jquery.mobilemenu.js"></script>
    <script src="landing/assets/js/jquery.cookie.js"></script>
    <script src="landing/assets/js/jquery.easing.1.3.js"></script>
    <script src="landing/assets/js/jquery.ui.totop.js"></script>
    <script src="landing/assets/js/jquery.touchSwipe.min.js"></script>
    <script src="landing/assets/js/jquery.equalheights.js"></script>
    
	@yield('head-scripts')
    <!--[if (gt IE 9)|!(IE)]><!-->
        <script src="landing/assets/js/jquery.mobile.customized.min.js"></script>
    <!--<![endif]-->
    
    <script>	
        $(window).load( function(){	
        	   jQuery('.camera_wrap').camera();	 
        });
    </script>   
    
    <!--[if lt IE 9]>
    <div style='text-align:center'><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/landing/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div>  
    <script src="landing/assets/landing/assets/landing/assets/js/html5shiv.js"></script> 
    <script src="landing/assets/landing/assets/landing/assets/js/respond.min.js"></script>
  <![endif]-->
</head>

<body>
<!--==============================header=================================-->
<header id="header">
      <div class="container">
          <h1 class="navbar-brand navbar-brand_"><a href="/"><img alt="Investment advisor" src="landing/assets/images/logo.png"></a></h1>
          <div class="menuheader">
            <nav class="navbar navbar-default navbar-static-top tm_navbar" role="navigation">
                <ul class="nav sf-menu">
                  <li><a href="/about-us">О нас</a></li>
                  <li><a href="/why">Почему мы?</a></li>
                  <li><a href="https://t.me/FRSProjectBot">Инвестировать</a></li>
                  <li><a href="/contacts">Контакты</a></li>
                </ul>
            </nav>
          </div>
      </div>
	  
	  @yield('content')
	  
	  <!--==============================footer=================================-->
<footer>
    <div class="container">
            <a class="smalllogo" href="/"><img alt="Investment advisor" src="landing/assets/images/smalllogo.png"></a>  
            <div class="">
                <p class="footerprivlink">&copy; 2018<br><a href="#"> Privacy policy</a>&nbsp;<!-- {%FOOTER_LINK} --></p>			<img src="landing/assets/images/p_systems.png" style="width:660px;" alt=""><strong>

            </div>
    </div>
</footer>
<script src="landing/assets/js/bootstrap.min.js"></script>
<script src="landing/assets/js/tm-scripts.js"></script>
</body></html>