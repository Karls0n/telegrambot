@extends('layouts.landing')

@section('head-scripts')
    <script src='landing/assets/js/TMForm.js'></script>
@endsection

@section('content')
<div id="content">
    <!--==============================row12=================================-->
    <div class="row_12" style="height:900px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 address">
                    <h2 class="color1">Мессенджеры</h2>
                    <address>
                        <div class="info">
                            <strong>Наша поддержка:</strong>
                            <p><a href="https://t.me/frsproject2018" target="_blank">@frsproject2018</a></p>
                            <strong>Наш чат телеграмм</strong>
                            <p><a href="https://t.me/joinchat/HzH6vkytQXuAB86V9_0juw" target="_blank">Присоединиться</a></p>
                        </div>
                    </address>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 address">
                    <h2 class="bottom1 color1">Задать вопрос</h2>
                    <form id="contact-form" class="contact-form">
                      <div class="success-message">Сообщение отправлено</div>
                      <div class="coll-1">
                          <label class="name">
                            <input type="text" placeholder="* Ваше имя:" data-constraints="@Required @JustLetters" />
                            <span class="empty-message">*Это поле обязательно.</span>
                            <span class="error-message">*Не верное значение поля name.</span>
                          </label>
                      </div>
                      <div class="coll-2">
                          <label class="email">
                            <input type="text" placeholder="* Ваш e-mail" data-constraints="@Required @Email" />
                            <span class="empty-message">*Это поле обязательно.</span>
                            <span class="error-message">*Не верное значение поля email.</span>
                          </label>
                      </div>
                      <div class="coll-3">
                          <label class="phone">
                            <input type="text" placeholder="Ваш номер телефона:" data-constraints="@JustNumbers"/>
                            <span class="empty-message">*Это поле обязательно.</span>
                            <span class="error-message">*Не верное значение поля phone.</span>
                          </label>
                      </div>
                      <label class="message">
                        <textarea placeholder="* Ваш вопрос или комментарий:" data-constraints='@Required @Length(min=20,max=999999)'></textarea>
                        <span class="empty-message">*Это поле обязательно.</span>
                        <span class="error-message">*Сообщение слишком короткое.</span>
                      </label>
                      <div>
                        <a href="#" data-type="submit" class="btn-link btn-link1 submitbutton">Отправить <i class="icon-ok-sign"></i></a>
                        <p class="req"><em>*</em>Обязательные поля</p>
                      </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection